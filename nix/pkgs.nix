let
  sources = import ./sources.nix;
  pkgsv = import sources.nixpkgs;
  pkgs = pkgsv {};
  cs-syd-site-pkgs = pkgsv {
    overlays = [
      (import (sources.safe-coloured-text + "/nix/overlay.nix"))
      (import (sources.yamlparse-applicative + "/nix/overlay.nix"))
      (import (sources.linkcheck + "/nix/overlay.nix"))
      (import (sources.seocheck + "/nix/overlay.nix"))
      (import (sources.cv-generator + "/nix/overlay.nix"))
      (import ./overlay.nix)
      (import ./gitignore-src.nix)
    ];
    config.allowUnfree = true;
  };
in
  cs-syd-site-pkgs
