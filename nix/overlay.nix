final: previous:
with final.haskell.lib;
let
  sources = import ./sources.nix;
in
{
  cs-syd-site = 
      final.stdenv.mkDerivation {
        name = "cs-syd-site";
        buildInputs = [
          final.haskellPackages.linkcheck
          final.haskellPackages.seocheck
          final.killall
        ];
        buildCommand = ''
          mkdir -p $out
          cp -r ${final.haskellPackages.site}/. $out

          $out/bin/site serve &
          sleep 0.1 # To give the site time to migrate the database
          linkcheck http://localhost:8000 --log-level Warn
          seocheck http://localhost:8000 --log-level Warn
          killall site
        '';
      };
  cs-syd-stylesheet = final.stdenv.mkDerivation {
                    name = "site-stylesheet.css";
                    src = final.gitignoreSource ../site/style/mybulma.scss;
                    buildInputs = [ final.sass ];
                    buildCommand = ''
                      # Dependencies: bulma and bulma-pricingtable
                      ln -s ${sources.bulma} bulma
                      ln -s ${sources.bulma-pricingtable} bulma-pricingtable
                      ln -s ${sources.bulma-carousel} bulma-carousel
                      ln -s ${sources.bulma-timeline} bulma-timeline

                      # The file we want to compile
                      # We need to copy this so that the relative path within it resolves to here instead of wherever we woudl link it from.
                      cp $src mybulma.scss
                      scss \
                        --sourcemap=none \
                        mybulma.scss:index.css --style compressed
                      cp index.css $out
                    '';
                  };


  haskellPackages =
    previous.haskellPackages.extend (
      self: super:
        let
          apiBuilderRepo = builtins.fetchGit {
              url = "https://github.com/intolerable/api-builder";
              rev = "f7440211195da98cfa93b6a97e51892b939919e1";
            };
          redditRepo = builtins.fetchGit {
              url = "https://github.com/intolerable/reddit";
              rev = "588901f79e684536a6839d63e6f9c6a4847fe001";
            };
          templateHaskellReloadRepo = builtins.fetchGit {
              url = "https://github.com/NorfairKing/template-haskell-reload";
              rev = "7111b945e3ae00ac48d905af1d925c138c334960";
            };
          yesodAutoReloadRepo = builtins.fetchGit {
              url = "https://github.com/NorfairKing/yesod-autoreload";
              rev = "f4f03bae0b9c1916838bb1c52a7182ac5afb28e0";
            };
          twitterRepo = builtins.fetchGit {
              url = "https://github.com/himura/twitter-conduit";
              rev = "c26a85c5201f6b0d9745ec5a88c8a21084693ae7";
            };
          twitterTypesRepo = builtins.fetchGit {
              url = "https://github.com/himura/twitter-types";
              rev = "dc8b6fe96a051fc9ff92b03c7e046e148b259189";
            };
          twitterTypesPkg = name: dontCheck (self.callCabal2nix name (twitterTypesRepo + "/${name}") {});
        in
          with final.haskellPackages;

          { site =
                addBuildDepend (dontHaddock (overrideCabal (disableLibraryProfiling (self.callCabal2nix "site" (final.gitignoreSource ../site) {})) (old:
                { preBuild = ''
                    export CV_DIR=${final.cvRelease}    
                    export LOGO_DIR=${sources.cs-syd-logo}    
                    export STYLE_FILE=${final.cs-syd-stylesheet}    
                  '';
                }))) self.autoexporter;
            template-haskell-reload = self.callCabal2nix "template-haskell-reload" (templateHaskellReloadRepo + "/template-haskell-reload") {};
            yesod-autoreload = self.callCabal2nix "yesod-autoreload" yesodAutoReloadRepo {};
            api-builder = dontCheck (self.callCabal2nix "api-builder" apiBuilderRepo {});
            reddit = dontCheck (self.callCabal2nix "reddit" redditRepo {});
            twitter-conduit = dontCheck (self.callCabal2nix "twitter-conduit" twitterRepo {});
            twitter-types = twitterTypesPkg "twitter-types";
            twitter-types-lens = twitterTypesPkg "twitter-types-lens";
          }
    );
}
