{ envname }:
{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.cs-syd."${envname}";

  mergeListRecursively = pkgs.callPackage ./merge-lists-recursively.nix { };
in
{
  options.services.cs-syd."${envname}" = {
    enable = mkEnableOption "CsSyd Website Service";
    default = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "Whether this should be the default nginx server";
    };
    envname = mkOption {
      type = types.str;
      default = "production";
      example = "production";
      description = "The name of the environment";
    };
    hosts = mkOption {
      type = types.listOf types.str;
      example = "cs-syd.eu";
      description = "The host to serve web requests on";
    };
    port = mkOption {
      type = types.int;
      default = 7900;
      example = 7900;
      description = "The port to serve web requests on";
    };
    tracking-id = mkOption {
      type = types.str;
      example = "UA-53296133-1";
      description = "The tracking id for google analytics";
    };
    verification-tag = mkOption {
      type = types.str;
      example = "ADkAx2F-JQO9KJBBdLfAGuJ_OMqPOsX5MdGDsfd0Ggw";
      description = "The verification tag for google search console";
    };
    pinterest-verification-tag = mkOption {
      type = types.nullOr types.str;
      example = "e1e0a1c20e33789b6eb302bc2a05efb3";
      description = "The verification tag for pinterest";
      default = null;
    };
    marketing-automation = mkOption {
      type = types.bool;
      example = true;
      description = "Wether to enable marketing automation";
    };
    reddit-credentials = mkOption {
      type = types.nullOr (
        types.submodule {
          options = {
            username = mkOption {
              type = types.str;
              example = true;
              description = "The reddit username";
            };
            password = mkOption {
              type = types.str;
              example = true;
              description = "The reddit password";
            };
            client-id = mkOption {
              type = types.str;
              example = true;
              description = "The reddit client id";
            };
            client-secret = mkOption {
              type = types.str;
              example = true;
              description = "The reddit client secret";
            };
          };
        }
      );
      default = null;
      description = "The reddit credentials";
    };
    twitter-credentials = mkOption {
      type = types.nullOr (
        types.submodule {
          options = {
            api-key = mkOption {
              type = types.str;
              example = "XXXXXXXXXXXXXXXXXXXXXXXXX";
              description = "The twitter api key";
            };
            api-secret = mkOption {
              type = types.str;
              example =
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
              description = "The twitter api secret";
            };
            access-token = mkOption {
              type = types.str;
              example =
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
              description = "The twitter access token";
            };
            access-token-secret = mkOption {
              type = types.str;
              example = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
              description = "The twitter access token secret";
            };
          };
        }
      );
      default = null;
      description = "The reddit credentials";
    };
    drafts = mkOption {
      type = types.bool;
      example = true;
      description = "Wether to enable drafts";
    };
  };
  config =
    let
      cs-syd-service =
        let
          workingDir = "/www/cs-syd.eu/${envname}";
          cs-syd-pkgs = import ../nix/pkgs.nix;
        in
        {
          description = "cs-syd.eu site ${envname} Service";
          wantedBy = [ "multi-user.target" ];
          environment = {
            "PORT" = "${builtins.toString (cfg.port)}";
            "APPROOT" = "https://${head (cfg.hosts)}";
            "TRACKING" = cfg.tracking-id;
            "SEARCH_CONSOLE_VERIFICATION" = cfg.verification-tag;
            "PINTEREST_VERIFICATION" = cfg.pinterest-verification-tag;
            "DRAFTS" = if cfg.drafts then "True" else "False";
            "MARKETING_AUTOMATION" = if cfg.marketing-automation then "True" else "False";
          } // (
            if isNull cfg.reddit-credentials then
              { }
            else
              (
                with cfg.reddit-credentials; {
                  "REDDIT_USERNAME" = username;
                  "REDDIT_PASSWORD" = password;
                  "REDDIT_CLIENT_ID" = client-id;
                  "REDDIT_CLIENT_SECRET" = client-secret;
                }
              )
          ) // (
            if isNull cfg.twitter-credentials then
              { }
            else
              (
                with cfg.twitter-credentials; {
                  "TWITTER_API_KEY" = api-key;
                  "TWITTER_API_SECRET" = api-secret;
                  "TWITTER_OAUTH_TOKEN" = access-token;
                  "TWITTER_OAUTH_TOKEN_SECRET" = access-token-secret;
                }
              )
          );
          script = ''
            mkdir -p "${workingDir}"
            cd "${workingDir}"
            ${cs-syd-pkgs.cs-syd-site}/bin/site \
              serve
          '';
          serviceConfig = {
            Restart = "always";
            RestartSec = 1;
            Nice = 15;
          };
        };
    in
    mkIf cfg.enable {
      systemd.services = { "cs-syd.eu-${envname}" = cs-syd-service; };
      networking.firewall.allowedTCPPorts = [ cfg.port ];
      services.nginx.virtualHosts =
        let redirectHost = host:
          nameValuePair "www.${host}" {
            enableACME = true;
            forceSSL = true;
            globalRedirect = host;
          };
        in
        optionalAttrs (cfg.enable && cfg.hosts != [ ]) ({
          "${head (cfg.hosts)}" = {
            enableACME = true;
            forceSSL = true;
            locations."/".proxyPass =
              "http://localhost:${builtins.toString (cfg.port)}";
            default = cfg.default;
            serverAliases = tail cfg.hosts;
          };
        } // builtins.listToAttrs (builtins.map redirectHost cfg.hosts));
    };
}
