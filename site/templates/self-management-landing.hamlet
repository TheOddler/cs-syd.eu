<div .box>
  <div .content>
    <p .title .is-1>
      Self-management training

  <section .hero>
    <div .hero-body>
        <div .title .has-text-centered>
          Learn how to eliminate stress and achieve a state of flow on demand.

  <section .section>
    <div .columns>
      <div .column .is-half .is-offset-one-quarter>
        <div class="pricing-table">
          <div .pricing-plan .is-active>
            <div .plan-header>Individual
            <div .plan-items>
              <div .plan-item>1:1 Coaching
              <div .plan-item>
                Designed for
                <strong>
                  knowledge workers
              <div .plan-item>On-site or Remote
              <div .plan-item>
                Lifetime
                <a href="https://intray.cs-syd.eu">
                  Intray
                membership
              <div .plan-item>
                Lifetime
                <a href="https://tickler.cs-syd.eu">
                  Tickler
                membership
              <div .plan-item>
                Lifetime
                <a href="https://smos.cs-syd.eu">
                  Smos
                membership

            <div .plan-footer>
              <a 
                .button .is-fullwidth .is-success
                href=https://calendly.com/cs-syd/introduction-call>
                Book


  <section .section>
    <p .title .is-4>
      Results
    <p .content .is-medium>
      As a knowledge worker, you should expect to learn to work stress free and to earn upwards of 10'000 USD more in the first five years alone.


  <section .section>
    <p .title .is-4>
      Meet the expert

    <p .content .is-medium>
      Tom Sydney Kerckhove is a life-long student of self-management.
      He has years of experience studying and teaching self-management.
      After trying out every self-management tool he could get his hands on (Todoist, Org-mode, Taskwarrior, Evernote, Notion, ...), he developed his own tools to combine the best aspects of each.
      Nowadays he spends his time leading teams, training knowledge workers in self-management and developing specialised new self-management tools.

    <p .content>
      Self-management has ben by far the most important thing I have learnt in my entire life.
      I could not imagine going back.
      - Tom Sydney Kerckhove
      
    <div style="max-width:460px;margin:auto;">
      <figure .image>
        <img .is-rounded src=@{AssetsStaticR speaking_headshot1_jpg} alt="headshot 1">


  <section .section>
    <p .title .is-4>
      Testimonials

    <div .tile .is-ancestor>
      <div .tile .is-vertical>
        <div .tile .is-horizontal>
          <div .tile .is-parent>
            <div .box .tile .is-child>
              <p .subtitle>
                I recommend self-management training to all engineers, managers and executives.
                I believe it has the potential to prevent costly mistakes via reduced stress, missed work-items via tracking and to make team members more productive.
                Trained team members will be more dependable and less likely to burn out.
              <p .has-text-right>
                Aniket Deshpande, Software Engineer

        <div .tile .is-parent>
          <div .box .tile .is-child>
            <p .subtitle>
              The CS SYD self-management training has had a significant effect on how I organize the inflow of new tasks and ideas into actionable items and to their execution. Most significant outcome from this has been the decrease of perceived stress as pressure has been shifted from memory dependent activities to standardized processes and specialized tools that support these processes. Both myself and my coworkers have also noticed that I manage my commitments better and I am better on time in my deliveries.

              I can highly recommend this training for any CEO who struggles with fast changing working environment with high levels of stress who wants to improve their self-management skills to get more done in the same time.

            <p .has-text-right>
              Samuli Kortelainen, CEO


  <section .section>
    <p .title .is-4>
      Training programme

    <p .title .is-5 .content>
      Part 1: Capture and the next action list

    <p .content>
      In the first part of the program, we focus on collecting all the
      <i>stuff
      in your life.
      We capture everything from the loose pieces of paper in yesterday's trousers to the thousands of emails on your third email account.
      We also set up a small system to get you started working through everything you have collected.

    <p .content>
      After part one, you will have an inbox system (Intray / paper basket / some other tool) and you will know how to use it.
      You will also have an organising system (Smos / paper / some other tool) set up and you will begin practicing using it.

    <p .content>
      You will be familiar with two preliminary rules.
      The first says that every piece of input that you allow into your life must go through your inbox system.
      The seconds says that you may only work on something if you find it on your next action list.

    <br>
    <p .title .is-5>
      Part 2: Context-based work and reviews

    <p>
      The second part of the program focuses on making your exocortex work to help you work stress-free.

    <p>
      You will learn to annotate your work with accurate metadata, and to use this metadata to decide what to work on.

    <p>
      You will become comfortable with not worrying about anything that you cannot help with anyway.

    <p>
      We will set you up with a review process that helps you keep your life current and well-aligned.


  <section .section>
    <div .box .has-text-centered>
      <p .title .is-3>
        Find out more now. Book a free call with Syd:
      <a .button .is-success .is-large
        href="https://calendly.com/cs-syd/introduction-call">
        Book Call Now
