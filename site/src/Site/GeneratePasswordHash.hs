module Site.GeneratePasswordHash where

import Import

import Data.Text.Encoding as TE

import Yesod.Auth.Util.PasswordStore

generatePasswordHash :: Text -> IO ()
generatePasswordHash pw = do
    h <- makePassword (TE.encodeUtf8 pw) 14
    print h
