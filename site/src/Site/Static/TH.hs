{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Site.Static.TH where

import CMarkGFM
import Data.Data
import qualified Data.Text as T
import Data.Time
import Import
import Instances.TH.Lift ()
import Language.Haskell.TH.Syntax
import qualified System.FilePath as FP

deriving instance Lift Day

data MdFile a = MdFile
  { mdTitle :: !Text,
    mdLastUpdated :: !(Maybe Day),
    mdAttributes :: [(Text, Text)],
    mdContents :: !Text,
    mdEstimatedReadingTime :: !Int,
    mdRendered :: !Text,
    mdExtra :: !a
  }
  deriving (Show, Eq, Generic, Data, Typeable, Lift, Functor)

mkValFunc :: (String -> Text -> [(Text, Text)] -> a) -> (Text -> Text -> MdFile a)
mkValFunc func urlText rawContents =
  let urlString = T.unpack urlText
      (attributes, contents) = splitContents rawContents
      att k = lookup k attributes
      maybeAtt k =
        case lookup k attributes of
          Nothing ->
            error $
              unlines
                [ "The post with url",
                  urlString,
                  "Does not have an attribute with key",
                  T.unpack k
                ]
          Just b -> b
      title = maybeAtt "title"
      mupdated =
        flip fmap (att "last-updated") $ \t ->
          case parseTimeM True defaultTimeLocale "%F" (T.unpack t) of
            Nothing -> error "Could not parse last-updated"
            Just lu -> lu
      rendered = renderMarkdown contents
      a = func urlString contents attributes
   in MdFile
        { mdTitle = title,
          mdLastUpdated = mupdated,
          mdAttributes = attributes,
          mdContents = contents,
          mdEstimatedReadingTime = estimateReadingTime contents,
          mdRendered = rendered,
          mdExtra = a
        }

data PostExtra = PostExtra
  { postTags :: ![Text],
    postDate :: !Day,
    postDescription :: Text,
    postExerpt :: !Text
  }
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type Post = MdFile PostExtra

sortPosts :: [(a, Post)] -> [(a, Post)]
sortPosts = sortOn (Down . postDate . mdExtra . snd)

{-# INLINE keyFunc #-}
keyFunc :: Path Rel File -> Text
keyFunc = T.pack . FP.dropExtension . toFilePath . filename

{-# INLINE postsValFunc #-}
postsValFunc :: Text -> Text -> Post
postsValFunc = mkValFunc $
  \urlString contents attributes ->
    let maybeAtt k =
          case lookup k attributes of
            Nothing ->
              error $
                unlines
                  [ "The post with url",
                    urlString,
                    "Does not have an attribute with key",
                    T.unpack k
                  ]
            Just a -> a
        tags = filter (not . T.null) . map T.strip . T.splitOn "," $ maybeAtt "tags"
     in case parseDayFromUrl urlString of
          Nothing -> error "unable to parse url day"
          Just day ->
            let rendered = renderMarkdown $ exerpt contents
             in PostExtra
                  { postTags = map T.toCaseFold tags,
                    postDate = day,
                    postDescription = exerpt contents,
                    postExerpt = rendered
                  }

exerpt :: Text -> Text
exerpt t =
  case T.splitOn "<!--more-->" t of
    (e : _) ->
      let s = T.strip e
       in T.strip $ fromMaybe s $ T.stripSuffix "<div></div>" s
    _ -> t

data QuoteExtra = QuoteExtra
  { quoteDescription :: Text,
    quoteQuote :: !Text,
    quoteAuthor :: !Text,
    quoteTags :: ![Text],
    quoteDate :: !Day,
    quoteExerpt :: !Text
  }
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type Quote = MdFile QuoteExtra

sortQuotes :: [(a, Quote)] -> [(a, Quote)]
sortQuotes = (sortOn (Down . quoteDate . mdExtra . snd))

quotesValFunc :: Text -> Text -> Quote
quotesValFunc = mkValFunc $
  \urlString contents attributes ->
    let maybeAtt k =
          case lookup k attributes of
            Nothing ->
              error $
                unlines
                  [ "The quote with url",
                    urlString,
                    "Does not have an attribute with key",
                    T.unpack k
                  ]
            Just a -> a
        quote = maybeAtt "quote"
        author = maybeAtt "author"
        tags = filter (not . T.null) . map T.strip . T.splitOn "," $ maybeAtt "tags"
     in case parseDayFromUrl urlString of
          Nothing -> error "unable to parse url day"
          Just day ->
            let description = exerpt contents
                rendered = renderMarkdown contents
             in QuoteExtra
                  { quoteDescription = description,
                    quoteQuote = quote,
                    quoteAuthor = author,
                    quoteTags = tags,
                    quoteDate = day,
                    quoteExerpt = rendered
                  }

data DraftExtra = DraftExtra {draftDescription :: !Text}
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type Draft = MdFile DraftExtra

sortDrafts :: [(a, Draft)] -> [(a, Draft)]
sortDrafts = sortOn (mdTitle . snd)

draftsValFunc :: Text -> Text -> Draft
draftsValFunc = mkValFunc $ \_ contents _ ->
  let description = exerpt contents
   in DraftExtra {draftDescription = description}

-- 5 characters per word
-- 200 words per minute on average
estimateReadingTime :: Text -> Int
estimateReadingTime = (`quot` (5 * 200)) . T.length

renderMarkdown :: Text -> Text
renderMarkdown contents =
  let opts = [optUnsafe]
      exts = []
      n = commonmarkToNode opts exts contents :: Node
   in nodeToHtml opts exts n

splitContents :: Text -> ([(Text, Text)], Text)
splitContents cs =
  let threeDashes = "---"
      parts = T.splitOn threeDashes cs
   in case parts of
        "" : ts : rest ->
          let attLines = T.lines ts
              tags =
                flip mapMaybe attLines $ \l ->
                  let column = ":"
                   in case T.splitOn column l of
                        [] -> Nothing
                        (key : valParts) ->
                          Just (key, T.intercalate column valParts)
              contents = T.intercalate threeDashes rest
           in (tags, contents)
        [contents] -> ([], contents)
        _ -> error $ "Failed to parse attributes in" <> T.unpack cs

parseDayFromUrl :: String -> Maybe Day
parseDayFromUrl =
  parseTimeM False defaultTimeLocale "%F" . take (T.length "2016-01-24")

data EntryExtra
  = PostEntry PostExtra
  | QuoteEntry QuoteExtra
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type Entry = MdFile EntryExtra

entryTags :: Entry -> [Text]
entryTags e =
  case mdExtra e of
    PostEntry p -> postTags p
    QuoteEntry q -> quoteTags q

entryDate :: Entry -> Day
entryDate e =
  case mdExtra e of
    PostEntry p -> postDate p
    QuoteEntry q -> quoteDate q

data TalkTemplateExtra = TalkTemplateExtra
  { talkTitle :: Text,
    talkDescription :: Text,
    talkCategory :: Text,
    talkAudience :: Text
  }
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type TalkTemplate = MdFile TalkTemplateExtra

sortTalkTemplates :: [(a, TalkTemplate)] -> [(a, TalkTemplate)]
sortTalkTemplates = sortOn (talkCategory . mdExtra . snd)

talkTemplatesValFunc :: Text -> Text -> TalkTemplate
talkTemplatesValFunc = mkValFunc $ \urlString _ attributes ->
  let maybeAtt k =
        case lookup k attributes of
          Nothing ->
            error $
              unlines
                [ "The talk template with url",
                  urlString,
                  "Does not have an attribute with key",
                  T.unpack k
                ]
          Just a -> a
      title = maybeAtt "title"
      description = maybeAtt "description"
      category = maybeAtt "category"
      audience = maybeAtt "audience"
   in TalkTemplateExtra
        { talkTitle = title,
          talkDescription = description,
          talkCategory = category,
          talkAudience = audience
        }

data PageExtra = PageExtra {pageDescription :: !Text}
  deriving (Show, Eq, Generic, Data, Typeable, Lift)

type Page = MdFile PageExtra

pagesValFunc :: Text -> Text -> Page
pagesValFunc = mkValFunc $ \urlString _ attributes ->
  let maybeAtt k =
        case lookup k attributes of
          Nothing ->
            error $
              unlines
                [ "The page with url",
                  urlString,
                  "Does not have an attribute with key",
                  T.unpack k
                ]
          Just a -> a
      description = maybeAtt "description"
   in PageExtra {pageDescription = description}
