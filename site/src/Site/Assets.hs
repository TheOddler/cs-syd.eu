{-# LANGUAGE TemplateHaskell #-}

module Site.Assets
  ( module Site.Assets,
    module Yesod.EmbeddedStatic,
  )
where

import Site.Constants
import Yesod.EmbeddedStatic

mkEmbeddedStatic
  development
  "staticAssets"
  [ embedDir "assets"
  ]
