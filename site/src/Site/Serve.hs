{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.Serve where

import Control.Concurrent (threadDelay)
import Control.Monad.Logger
import qualified Data.Text as T
import Database.Persist.Sqlite
import Network.HTTP.Client.TLS as HTTP
import Network.HTTP.Types as HTTP
import Network.Wai as Wai
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.RequestLogger as Wai
import Site.Application ()
import Site.Foundation
import Site.Marketing
import Site.OptParse
import UnliftIO

serve :: ServeSettings -> IO ()
serve serveSets@ServeSettings {..} = runStderrLoggingT $
  withSqlitePoolInfo serveSetConnectionInfo 1 $ \pool -> do
    runSqlPool (runMigration siteMigration) pool
    man <- HTTP.newTlsManager
    let site =
          Site
            { siteLogo = staticLogo,
              siteStyle = staticStyle,
              siteAssets = staticAssets,
              siteCv = staticCv,
              siteApproot = serveSetApproot,
              siteTracking = serveSetTracking,
              siteVerification = serveSetVerification,
              sitePinterestVerification = serveSetPinterestVerification,
              siteDrafts = serveSetDrafts,
              siteConnectionPool = pool
            }

    let mMenv = do
          rar <- serveSetApproot
          guard serveSetMarketingAutomation
          pure $
            MarketingEnv
              { marketingEnvConnectionPool = pool,
                marketingEnvUrlRenderer = yesodRender site rar,
                marketingEnvHTTPManager = man,
                marketingEnvTwitterCreds = serveSetTwitterCreds,
                marketingEnvRedditCreds = serveSetRedditCreds
              }

    let runServer :: LoggingT IO ()
        runServer = liftIO $ do
          putStrLn $ unlines ["Serving with these settings", ppShow serveSets]

          siteApp <- toWaiAppPlain site
          Warp.run serveSetPort $ customMiddleware siteApp
    concurrently_ runServer $ do
      -- Wait for the server to come online before we do the marketing
      -- just in case the server is not ready before the marketing starts
      liftIO $ threadDelay 5_000_000
      mapM_ runMarketingAutomation mMenv

customMiddleware :: Wai.Middleware
customMiddleware =
  retryWithoutHtmlMiddleware
    . ( if development
          then Wai.logStdoutDev
          else Wai.logStdout
      )
    . defaultMiddlewaresNoLogging

retryWithoutHtmlMiddleware :: Wai.Middleware
retryWithoutHtmlMiddleware doRequest request sendResp =
  doRequest request $ \response ->
    if responseStatus response /= notFound404
      then sendResp response
      else case dropHtmlExtension $ pathInfo request of
        Nothing -> sendResp response
        Just pathInfo' ->
          let request' = request {pathInfo = pathInfo'}
           in doRequest request' sendResp

-- 'Nothing' means "Don't retry at all"
dropHtmlExtension :: [Text] -> Maybe [Text]
dropHtmlExtension ts =
  case reverse ts of
    [] -> Nothing
    (part : rest) ->
      case T.stripSuffix ".html" part of
        Nothing -> Nothing
        Just stripped -> Just $ reverse $ stripped : rest
