{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Site.Foundation
  ( module Import,
    module Site.Style,
    module Site.Logo,
    module Site.CV,
    module Site.DB,
    module Site.Assets,
    module Site.Constants,
    module Site.Foundation,
    module Site.Static,
    module Site.Widget,
    module Yesod,
    module Yesod.Auth,
    module Language.Haskell.TH.Load,
  )
where

import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.Time
import Database.Persist
import Database.Persist.Sql
import Import
import Language.Haskell.TH.Load
import Site.Assets
import Site.CV
import Site.Constants
import Site.DB
import Site.Logo
import Site.Static
import Site.Style
import Site.Widget
import Text.Hamlet
import Yesod
import Yesod.Auth
import Yesod.Auth.Hardcoded
import Yesod.Auth.Message
import Yesod.Auth.Util.PasswordStore
import Yesod.AutoReload

data Site = Site
  { siteLogo :: EmbeddedStatic,
    siteStyle :: EmbeddedStatic,
    siteAssets :: EmbeddedStatic,
    siteCv :: EmbeddedStatic,
    siteApproot :: Maybe Text,
    siteTracking :: Maybe Text,
    siteVerification :: Maybe Text,
    sitePinterestVerification :: Maybe Text,
    siteDrafts :: Bool,
    siteConnectionPool :: ConnectionPool
  }

mkYesodData "Site" $(parseRoutesFile "routes")

instance Yesod Site where
  approot = ApprootMaster $ fromMaybe "" . siteApproot
  defaultLayout widget = do
    site <- getYesod
    let addReloadWidget = if development then (<> autoReloadWidgetFor WebSocketR) else id
    pageContent <-
      widgetToPageContent $ do
        addStylesheet $ StyleR index_css
        let navbar = $(widgetFile "navbar")
        addReloadWidget $(widgetFile "default-body")
    withUrlRenderer $(hamletFile "templates/default-page.hamlet")
  isAuthorized (AdminR _) _ =
    if development
      then pure Authorized
      else do
        maid <- maybeAuthId
        pure $
          case maid of
            Just _ -> Authorized
            Nothing -> AuthenticationRequired
  isAuthorized _ _ = pure Authorized
  authRoute _ = Just $ AuthR LoginR

instance RenderMessage Site FormMessage where
  renderMessage _ _ = defaultFormMessage

data Admin = Admin
  { adminUserName :: Text,
    adminPasswordHash :: ByteString
  }
  deriving (Show)

siteAdmins :: [Admin]
siteAdmins =
  [ Admin "syd" "sha256|14|JRASMogsU/WI6DaGXjgP+w==|aW31955zr+l5JRBUGEPvFsE565Kt5aNSZWfs7XTRHz0=",
    Admin "syd" "sha256|14|CfboyTkooq7kZ2kWk65IGQ==|Trzxd+KcbKsUbCPHOgwCNOefKQ9vVJtNAt0hN/b+xN8=",
    Admin "syd" "sha256|14|HqetrdxoVGB5iFNkceYGXg==|XgoeoM7OwO/0EVAtMAxYKt7BC7RRdVrZcCo1Z8XUfeU=",
    Admin "syd" "sha256|14|jZPR0m8CnhZrKURgaLRM7Q==|hloB1g+NsavKkZQZfOJyiPp9J7IFUaAJ5hbKAdBHRc0="
  ]

instance YesodAuth Site where
  type AuthId Site = Text
  authPlugins _ = [authHardcoded]
  authenticate Creds {..} =
    pure $
      case credsPlugin of
        "hardcoded" ->
          case lookupUser credsIdent of
            Nothing -> UserError InvalidLogin
            Just m -> Authenticated $ adminUserName m
        _ -> ServerError "Invalid auth plugin"
  loginDest _ = AdminR PanelR
  logoutDest _ = HomeR
  maybeAuthId = do
    mc <- lookupSession credsKey
    pure $ do
      ck <- mc
      t <- fromPathPiece ck
      _ <- lookupUser t
      pure t

instance YesodAuthHardcoded Site where
  validatePassword u = return . traceShowId . validPassword u
  doesUserNameExist = return . isJust . lookupUser

lookupUser :: Text -> Maybe Admin
lookupUser username = find (\m -> adminUserName m == username) siteAdmins

validPassword :: Text -> Text -> Bool
validPassword u p =
  case find (\m -> adminUserName m == u) siteAdmins of
    Just a -> verifyPassword (TE.encodeUtf8 p) $ adminPasswordHash a
    _ -> False

withTitle :: Text -> Widget
withTitle title = setTitle $ toHtml $ T.concat ["CS SYD - ", title]

preTime :: Day -> Widget
preTime d =
  toWidget [shamlet|<span style="font-family: monospace;">#{formatTime defaultTimeLocale "%F" d}|]

dateTag :: Day -> Widget
dateTag d =
  toWidget
    [shamlet|
        <div .tags .has-addons>
          <span .tag .is-info>
            Date
          <span .tag>
            #{formatTime defaultTimeLocale "%F" d}|]

ertTag :: Int -> Widget
ertTag ert =
  toWidget
    [shamlet|
        <div .tags .has-addons>
          <a .tag .is-info
            href="/posts/2016-06-05-estimated-reading-time-in-hakyll">
            ERT
          <a .tag
            href="/posts/2016-06-05-estimated-reading-time-in-hakyll">
              $if ert < 1
                \< 1 min
              $else
                #{ert} min
        |]

entryBox :: Text -> Entry -> Widget
entryBox url md = $(widgetFile "entry-box")

data ActionOption
  = Speaking
  | SelfManagement
  | Consulting
  | Templates
  deriving (Show, Eq, Ord)

chooseTagsAction :: [Text] -> Maybe ActionOption
chooseTagsAction = minimumMay . mapMaybe chooseTagAction

chooseTagAction :: Text -> Maybe ActionOption
chooseTagAction =
  \case
    "talk" -> Just Speaking
    "speaking" -> Just Speaking
    "taskwarrior" -> Just SelfManagement
    "org mode" -> Just SelfManagement
    "productivity" -> Just SelfManagement
    "gtd" -> Just SelfManagement
    "self-management" -> Just SelfManagement
    "templates" -> Just Templates
    "haskell" -> Just Templates
    "programming" -> Just Consulting
    "validity" -> Just Consulting
    _ -> Nothing

actionOptionButton :: Maybe ActionOption -> Widget
actionOptionButton = \case
  Just SelfManagement -> selfManagementButton
  Just Speaking -> speakingButton
  Just Consulting -> consultingButton
  Just Templates -> templatesButton
  Nothing -> servicesButton

servicesButton :: Widget
servicesButton = $(widgetFile "services-button")

selfManagementButton :: Widget
selfManagementButton = $(widgetFile "self-management-button")

speakingButton :: Widget
speakingButton = $(widgetFile "speaking-button")

consultingButton :: Widget
consultingButton = $(widgetFile "consulting-button")

templatesButton :: Widget
templatesButton = $(widgetFile "templates-button")

findNextAndPrevious :: (Eq a) => a -> (e -> a) -> [e] -> (Maybe e, Maybe e)
findNextAndPrevious name func es =
  let mPrev = do
        ix <- findIndex ((== name) . func) es
        atMay es $ ix - 1
      mNext = do
        ix <- findIndex ((== name) . func) es
        atMay es $ ix + 1
   in (mPrev, mNext)

getPage :: Text -> Handler Page
getPage url = do
  mP <- loadIO $ findPage url
  case mP of
    Nothing -> notFound
    Just p -> pure p
