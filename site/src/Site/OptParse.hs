{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.OptParse
  ( getInstructions,
    Instructions (..),
    Dispatch (..),
    ServeSettings (..),
    Settings (..),
  )
where

import Data.String (IsString (..))
import qualified Data.Text as T
import Database.Persist.Sqlite
import Import
import Options.Applicative
import Reddit
import Site.OptParse.Types
import System.Environment (getArgs, getEnvironment)
import System.Exit (die)
import Text.Read
import qualified Web.Twitter.Conduit as Twitter

getInstructions :: IO Instructions
getInstructions = do
  Arguments cmd flags <- getArguments
  env <- getEnv
  config <- getConfiguration cmd flags env
  combineToInstructions cmd flags env config

combineToInstructions ::
  Command -> Flags -> Environment -> Configuration -> IO Instructions
combineToInstructions cmd Flags Environment {..} Configuration =
  case cmd of
    (CommandServe ServeFlags {..}) -> do
      let serveSetPort = fromMaybe 8000 $ serveFlagPort `mplus` envPort
      let serveSetConnectionInfo =
            mkSqliteConnectionInfo $
              fromMaybe "site.db" $
                serveFlagDb `mplus` envDb
      let serveSetApproot = serveFlagApproot `mplus` envApproot
      let serveSetTracking = serveFlagTracking `mplus` envTracking
      let serveSetVerification = serveFlagVerification `mplus` envVerification
      let serveSetPinterestVerification =
            serveFlagPinterestVerification `mplus` envPinterestVerification
      let serveSetDrafts = fromMaybe False $ serveFlagDrafts `mplus` envDrafts
      let serveSetMarketingAutomation = fromMaybe False envMarketingAutomation
      let serveSetRedditCreds = envRedditCreds
      let serveSetTwitterCreds = envTwitterCreds
      pure $ Instructions (DispatchServe ServeSettings {..}) Settings
    CommandGeneratePasswordHash pw ->
      pure $ Instructions (DispatchGeneratePasswordHash pw) Settings

getConfiguration :: Command -> Flags -> Environment -> IO Configuration
getConfiguration _ _ _ = pure Configuration

getEnv :: IO Environment
getEnv = do
  env <- getEnvironment
  let ms k = fromString <$> lookup k env
      mr k =
        forM (ms k) $ \s ->
          case readMaybe s of
            Nothing -> die $ unwords ["Could not read", show s]
            Just v -> pure v
  envPort <- mr "PORT"
  let envDb = ms "DATABASE" `mplus` ms "DATABASE_CONNECTION_STRING"
  let envApproot = ms "APPROOT"
  let envTracking = ms "TRACKING"
  let envVerification = ms "SEARCH_CONSOLE_VERIFICATION"
  let envPinterestVerification = ms "PINTEREST_VERIFICATION"
  envMarketingAutomation <- mr "MARKETING_AUTOMATION"
  let envRedditCreds =
        Reddit.Credentials <$> ms "REDDIT_USERNAME" <*> ms "REDDIT_PASSWORD"
          <*> (ClientParams <$> ms "REDDIT_CLIENT_ID" <*> ms "REDDIT_CLIENT_SECRET")
  let envTwitterCreds = do
        apiKey <- ms "TWITTER_API_KEY"
        apiSecret <- ms "TWITTER_API_SECRET"
        oauthToken <- ms "TWITTER_OAUTH_TOKEN"
        oauthTokenSecret <- ms "TWITTER_OAUTH_TOKEN_SECRET"
        let tokens =
              Twitter.twitterOAuth
                { Twitter.oauthConsumerKey = apiKey,
                  Twitter.oauthConsumerSecret = apiSecret
                }
        let credential =
              Twitter.Credential
                [ ("oauth_token", oauthToken),
                  ("oauth_token_secret", oauthTokenSecret)
                ]
        pure $
          Twitter.def
            { Twitter.twToken =
                Twitter.def
                  { Twitter.twOAuth = tokens,
                    Twitter.twCredential = credential
                  },
              Twitter.twProxy = Nothing
            }
  envDrafts <- mr "DRAFTS"
  pure Environment {..}

getArguments :: IO Arguments
getArguments = do
  args <- getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser = execParserPure prefs_ argParser
  where
    prefs_ =
      defaultPrefs
        { prefShowHelpOnError = True,
          prefShowHelpOnEmpty = True
        }

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) help_
  where
    help_ = fullDesc <> progDesc description
    description = "CS SYD server"

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand =
  hsubparser $
    mconcat
      [ command "serve" parseCommandServe,
        command "generate-password-hash" parseCommandGeneratePasswordHash
      ]

parseCommandServe :: ParserInfo Command
parseCommandServe = info parser modifier
  where
    parser =
      CommandServe
        <$> ( ServeFlags
                <$> option
                  (Just <$> auto)
                  ( mconcat
                      [ long "port",
                        value Nothing,
                        metavar "PORT",
                        help "the port to serve on"
                      ]
                  )
                <*> option
                  (Just . T.pack <$> str)
                  ( mconcat
                      [ long "database",
                        value Nothing,
                        metavar "DATABASE_CONNECTION_STRING",
                        help "The sqlite connection string"
                      ]
                  )
                <*> option
                  (Just . T.pack <$> str)
                  ( mconcat
                      [ long "approot",
                        value Nothing,
                        metavar "APPROOT",
                        help "The app root to use"
                      ]
                  )
                <*> option
                  (Just . T.pack <$> str)
                  ( mconcat
                      [ long "analytics-tracking-id",
                        value Nothing,
                        metavar "TRACKING_ID",
                        help "The google analytics tracking ID"
                      ]
                  )
                <*> option
                  (Just . T.pack <$> str)
                  ( mconcat
                      [ long "search-console-verification",
                        value Nothing,
                        metavar "VERIFICATION_TAG",
                        help "The contents of the google search console verification tag"
                      ]
                  )
                <*> option
                  (Just . T.pack <$> str)
                  ( mconcat
                      [ long "pinterest-verification",
                        value Nothing,
                        metavar "PINTEREST_VERIFICATION_TAG",
                        help "The contents of the pinterest verification tag"
                      ]
                  )
                <*> onOffFlag "" (mconcat [long "drafts", help "render and serve drafts"])
            )
    modifier = fullDesc <> progDesc "Serve the site."

parseCommandGeneratePasswordHash :: ParserInfo Command
parseCommandGeneratePasswordHash = info parser modifier
  where
    parser =
      CommandGeneratePasswordHash
        <$> argument
          (T.pack <$> str)
          (mconcat [metavar "PASSWORD", help "The password to hash"])
    modifier = fullDesc <> progDesc "Generate a password hash."

parseFlags :: Parser Flags
parseFlags = pure Flags

onOffFlag :: String -> Mod FlagFields (Maybe Bool) -> Parser (Maybe Bool)
onOffFlag suffix mods =
  flag' (Just True) (mconcat [long $ pf "enable", hidden])
    <|> flag' (Just False) (mconcat [long $ pf "disable", hidden])
    <|> flag' Nothing (mconcat [long ("(enable|disable)-" ++ suffix), mods])
    <|> pure Nothing
  where
    pf s = intercalate "-" [s, suffix]
