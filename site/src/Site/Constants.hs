{-# LANGUAGE CPP #-}

module Site.Constants where

import Language.Haskell.TH.Load

#ifdef DEVELOPMENT
development :: Bool
development = True
#else
development :: Bool
development = False
#endif

mode :: Mode
mode = if development then LoadLive else BakeIn
