{-# OPTIONS_GHC -ddump-splices #-}

module Site.CV.TH where

import Language.Haskell.TH
import Site.Constants
import System.Environment
import System.Exit
import Yesod.EmbeddedStatic

mkEmbeddedCv :: Q [Dec]
mkEmbeddedCv = do
  let baseFiles =
        [ "fail.pdf",
          "for-recruiters.pdf",
          "for-engineers.pdf",
          "cv.pdf"
        ]
  md <- runIO $ lookupEnv "CV_DIR"
  ncv <- runIO $ lookupEnv "NO_CV"
  fs <-
    case md of
      Nothing -> case ncv of
        Nothing -> runIO $ die "No CV found."
        Just _ -> do
          runIO $ putStrLn "WARNING: Not including CV."
          pure []
      Just d -> do
        runIO $ putStrLn $ unwords ["Including CV in dir: ", d]
        pure $ map (\p -> embedFileAt p $ d ++ "/" ++ p) baseFiles
  mkEmbeddedStatic development "staticCv" fs
