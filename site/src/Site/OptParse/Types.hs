module Site.OptParse.Types where

import Database.Persist.Sqlite
import Import
import Reddit
import Web.Twitter.Conduit as Twitter

data Arguments
  = Arguments Command Flags
  deriving (Show, Eq)

data Instructions
  = Instructions Dispatch Settings
  deriving (Show)

data Command
  = CommandServe ServeFlags
  | CommandGeneratePasswordHash Text
  deriving (Show, Eq)

data ServeFlags = ServeFlags
  { serveFlagPort :: Maybe Int,
    serveFlagDb :: Maybe Text,
    serveFlagApproot :: Maybe Text,
    serveFlagTracking :: Maybe Text,
    serveFlagVerification :: Maybe Text,
    serveFlagPinterestVerification :: Maybe Text,
    serveFlagDrafts :: Maybe Bool
  }
  deriving (Show, Eq)

data Flags
  = Flags
  deriving (Show, Eq)

data Environment = Environment
  { envPort :: Maybe Int,
    envDb :: Maybe Text,
    envApproot :: Maybe Text,
    envTracking :: Maybe Text,
    envVerification :: Maybe Text,
    envPinterestVerification :: Maybe Text,
    envDrafts :: Maybe Bool,
    envMarketingAutomation :: Maybe Bool,
    envRedditCreds :: Maybe Reddit.LoginMethod,
    envTwitterCreds :: Maybe Twitter.TWInfo
  }
  deriving (Show)

data Configuration
  = Configuration
  deriving (Show, Eq)

data Dispatch
  = DispatchServe ServeSettings
  | DispatchGeneratePasswordHash Text
  deriving (Show)

data Settings
  = Settings
  deriving (Show, Eq)

data ServeSettings = ServeSettings
  { serveSetPort :: Int,
    serveSetConnectionInfo :: SqliteConnectionInfo,
    serveSetApproot :: Maybe Text,
    serveSetTracking :: Maybe Text,
    serveSetVerification :: Maybe Text,
    serveSetPinterestVerification :: Maybe Text,
    serveSetDrafts :: Bool,
    serveSetMarketingAutomation :: Bool,
    serveSetRedditCreds :: Maybe Reddit.LoginMethod,
    serveSetTwitterCreds :: Maybe Twitter.TWInfo
  }
  deriving (Show)
