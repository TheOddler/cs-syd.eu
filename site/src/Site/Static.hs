{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Static
  ( Draft,
    DraftExtra (..),
    Entry,
    EntryExtra (..),
    Page,
    PageExtra (..),
    Post,
    PostExtra (..),
    Quote,
    QuoteExtra (..),
    TalkTemplate,
    TalkTemplateExtra (..),
    entryDate,
    entryTags,
    load,
    module Site.Static,
    sortDrafts,
    sortPosts,
    sortQuotes,
    sortTalkTemplates,
    MdFile (..),
  )
where

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Import
import Language.Haskell.TH.Load
import Site.Constants
import Site.Static.TH

sortEntries :: [(a, Entry)] -> [(a, Entry)]
sortEntries = sortOn (Down . entryDate . snd)

getEntries :: Load (Map Text Entry)
getEntries =
  liftA2
    M.union
    (fmap (M.map (fmap PostEntry)) getPosts)
    (fmap (M.map (fmap QuoteEntry)) getQuotes)

getPosts :: Load (Map Text Post)
getPosts = $$(embedTextFilesInWith keyFunc [||keyFunc||] postsValFunc [||postsValFunc||] mode [reldir|content/posts|])

getQuotes :: Load (Map Text Quote)
getQuotes = $$(embedTextFilesInWith keyFunc [||keyFunc||] quotesValFunc [||quotesValFunc||] mode [reldir|content/quotes|])

getTalkTemplates :: Load (Map Text TalkTemplate)
getTalkTemplates = $$(embedTextFilesInWith keyFunc [||keyFunc||] talkTemplatesValFunc [||talkTemplatesValFunc||] mode [reldir|content/talk-templates|])

getDrafts :: Load (Map Text Draft)
getDrafts = $$(embedTextFilesInWith keyFunc [||keyFunc||] draftsValFunc [||draftsValFunc||] mode [reldir|content/drafts|])

getPages :: Load (Map Text Page)
getPages = $$(embedTextFilesInWith keyFunc [||keyFunc||] pagesValFunc [||pagesValFunc||] mode [reldir|content/pages|])

findPage :: Text -> Load (Maybe Page)
findPage url = M.lookup url <$> getPages

getTags :: Load [Text]
getTags = M.keys <$> tagsMap

tagsMap :: Load (Map Text [Entry])
tagsMap = listMap entryTags . M.elems <$> getEntries

getTagSizesMap :: Load [(Text, Text)]
getTagSizesMap = countMapSizes <$> tagCountMap

tagCountMap :: Load (Map Text Int)
tagCountMap = countMap entryTags . M.elems <$> getEntries

countMapSizes :: Map Text Int -> [(Text, Text)]
countMapSizes = map (second go) . M.toList
  where
    go :: Int -> Text
    go x
      | x <= 3 = "is-small"
      | x <= 6 = "is-medium"
      | x <= 9 = "is-large"
      | otherwise = "is-large is-info .is-inverted"

countMap :: Ord b => (a -> [b]) -> [a] -> Map b Int
countMap func = M.map length . listMap func

listMap ::
  forall a b.
  Ord b =>
  (a -> [b]) ->
  [a] ->
  Map b [a]
listMap func = foldl go M.empty
  where
    go :: Map b [a] -> a -> Map b [a]
    go m a = foldl go2 m $ func a
      where
        go2 :: Map b [a] -> b -> Map b [a]
        go2 m2 b =
          M.alter
            ( \case
                Nothing -> Just [a]
                Just as -> Just $! a : as
            )
            b
            m2
