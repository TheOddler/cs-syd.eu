module Site.Style.TH where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Path
import Path.IO
import Site.Constants
import System.Environment
import System.Exit
import System.Process
import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Types

styleGenerator :: Generator
styleGenerator = do
  md <- runIO $ lookupEnv "STYLE_FILE"
  ns <- runIO $ lookupEnv "NO_STYLE"
  case md of
    Nothing -> case ns of
      Nothing -> runIO $ die "No style dir found."
      Just _ -> do
        d <- resolveDir' "style"
        runIO $ putStrLn $ unwords ["WARNING: Including style files from submodule at path: ", fromAbsDir d]
        scssFile <- resolveFile d "mybulma.scss"
        cssFile <- resolveFile d "mybulma.css"
        qAddDependentFile $ fromAbsFile scssFile
        runIO $ do
          putStrLn $ unwords ["Regenerating the stylesheet at", fromAbsFile cssFile]
          ec <-
            system $
              unwords
                [ "sass",
                  "--sourcemap=none",
                  concat [fromAbsFile scssFile, ":", fromAbsFile cssFile],
                  if development then "" else "--style compressed"
                ]
          case ec of
            ExitSuccess -> pure ()
            ExitFailure i -> die $ "Regenerating the stylesheet failed with exit code: " <> show i
        embedFileAt "index.css" $ fromAbsFile cssFile
    Just f -> do
      runIO $ putStrLn $ unwords ["Including style in : ", f]
      embedFileAt "index.css" f
