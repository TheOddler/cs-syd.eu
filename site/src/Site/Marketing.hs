{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Site.Marketing where

import Control.Monad.Logger
import Control.Monad.Reader
import Data.Map (Map)
import qualified Data.Map as M
import Data.Set (Set)
import qualified Data.Text as T
import Data.Time
import Database.Persist
import Database.Persist.Sql
import Language.Haskell.TH.Load
import Network.HTTP.Client as HTTP
import qualified Reddit
import Site.Foundation
import Text.Show.Pretty
import Web.Twitter.Conduit as Twitter
import Web.Twitter.Conduit.Status as Twitter
import Web.Twitter.Types as Twitter

type M = ReaderT MarketingEnv (LoggingT IO)

data MarketingEnv = MarketingEnv
  { marketingEnvConnectionPool :: !ConnectionPool,
    marketingEnvUrlRenderer :: !(Route Site -> [(Text, Text)] -> Text),
    marketingEnvHTTPManager :: !HTTP.Manager,
    marketingEnvTwitterCreds :: !(Maybe Twitter.TWInfo),
    marketingEnvRedditCreds :: !(Maybe Reddit.LoginMethod)
  }

runM :: MarketingEnv -> M a -> LoggingT IO a
runM = flip runReaderT

runMDB :: SqlPersistT IO a -> M a
runMDB func = do
  pool <- asks marketingEnvConnectionPool
  liftIO $ runSqlPool func pool

runMarketingAutomation :: MarketingEnv -> LoggingT IO ()
runMarketingAutomation = runReaderT runMarketing

runMarketing :: M ()
runMarketing = do
  entries <- liftIO getEntriesToMarket
  mapM_ (uncurry runMarketingFor) (M.toList entries)

getEntriesToMarket :: IO (Map Text Entry)
getEntriesToMarket = do
  entries <- loadIO getEntries
  today <- liftIO $ utctDay <$> getCurrentTime
  -- Only (possibly) do marketing for entries that are supposed to be published today.
  -- This prevents us from doing marketing for all the posts from the past years.
  -- It also prevents us from doing marketing for subreddits that we haven't posted to in the past
  pure $ M.filter ((today ==) . entryDate) entries

runMarketingFor :: Text -> Entry -> M ()
runMarketingFor url entry = do
  logInfoNS "MARKETING" $ "Running marketing automation for: " <> url
  runTwitterMarketingFor url entry
  runRedditMarketingForUrl url entry

runTwitterMarketingFor :: Text -> Entry -> M ()
runTwitterMarketingFor url entry = do
  mTwInfo <- asks marketingEnvTwitterCreds
  forM_ mTwInfo $ \creds -> do
    mTs <- runMDB $ getBy $ UniqueTwitterStatus url
    case mTs of
      Just _ -> pure () -- Already posted, don't do it again.
      Nothing -> do
        logInfoNS "MARKETING" $ "Running Twitter marketing automation for: " <> url
        renderer <- asks marketingEnvUrlRenderer
        man <- asks marketingEnvHTTPManager
        let link = renderer (entryUrl url entry) [("source", "twitter")]
        let tweet =
              T.unlines
                [ "New CS SYD Blogpost!",
                  "",
                  T.strip $ mdTitle entry,
                  "",
                  link
                ]
        logInfoNS "MARKETING" $ "Tweeting:\n" <> tweet
        status <- liftIO $ Twitter.call creds man $ Twitter.update tweet
        liftIO $ pPrint status
        runMDB $
          insert_ $
            TwitterStatus
              { twitterStatusStatus = fromIntegral $ Twitter.statusId status,
                twitterStatusEntry = url
              }

entryUrl :: Text -> Entry -> Route Site
entryUrl url entry =
  case mdExtra entry of
    PostEntry _ -> PostsR url
    QuoteEntry _ -> QuoteR url

runRedditMarketingForUrl :: Text -> Entry -> M ()
runRedditMarketingForUrl url entry = do
  mRedditCreds <- asks marketingEnvRedditCreds
  forM_ mRedditCreds $ \creds -> do
    let subreddits = subredditsFor entry
    forM_ subreddits $ \subreddit -> do
      mRedditPost <- runMDB $ getBy $ UniqueRedditPost subreddit url
      case mRedditPost of
        Just _ -> pure () -- Already posted, don't do it again
        Nothing -> do
          logInfoNS "MARKETING" $ "Running Reddit marketing automation for: " <> url <> " for subreddit: " <> subreddit
          renderer <- asks marketingEnvUrlRenderer
          let ros = def {Reddit.customUserAgent = Just "CS SYD site admin bot", Reddit.loginMethod = creds}
          errOrPostId <-
            Reddit.runRedditWith ros $ do
              let title = T.unwords ["CS SYD -", mdTitle entry]
              let link = renderer (entryUrl url entry) [("source", "reddit")]
              Reddit.submitLink (Reddit.R subreddit) title link
          liftIO $ pPrint errOrPostId
          case errOrPostId of
            Left err -> logErrorNS "MARKETING" (T.pack (show err))
            Right (Reddit.PostID pid) ->
              runMDB $
                insert_
                  RedditPost
                    { redditPostSubreddit = subreddit,
                      redditPostPost = pid,
                      redditPostEntry = url
                    }

subredditsFor :: Entry -> Set Text
subredditsFor = foldMap subredditsForTag . entryTags

subredditsForTag :: Text -> Set Text
subredditsForTag = \case
  "haskell" -> ["haskell"]
  "nix" -> ["nixos"]
  _ -> []
