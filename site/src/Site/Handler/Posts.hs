{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Posts
  ( getPostsR,
  )
where

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import Site.Foundation

getPostsR :: Text -> Handler Html
getPostsR url = do
  posts <- loadIO getPosts
  entries <- loadIO getEntries
  case M.lookup url posts of
    Nothing -> do
      quotes <- loadIO getQuotes
      case M.lookup url quotes of
        Nothing -> notFound
        Just _ -> redirect $ QuoteR url
    Just md -> do
      let (mPrevE, mNextE) = findNextAndPrevious url fst $ sortEntries $ M.toList entries
      neverExpires
      defaultLayout $ do
        setTitle $ toHtml $ T.unwords ["CS SYD", "-", mdTitle md]
        toWidgetHead
          [hamlet|<meta name="description" content="#{postDescription $ mdExtra md}"/>|]
        -- Syntax highlighting
        addStylesheetRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/haskell.min.js"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/nix.min.js"
        -- Maths
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
        $(widgetFile "post")
