{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.SelfManagement where

import Data.Time
import Site.Foundation

getSelfManagementR :: Handler Html
getSelfManagementR = do
  today <- liftIO $ utctDay <$> getCurrentTime
  let weeks 1 = "1 week"
      weeks n = show n <> " weeks"
      timelineDay n =
        formatTime defaultTimeLocale ("%e %B %Y" <> if n /= 0 then (", in " <> weeks n) else "") $ addDays (n * 7) today
  expiresAt (UTCTime (addDays 1 today) 0)
  defaultLayout $ do
    withTitle "Self-Management"
    toWidgetHead [hamlet|<meta name="description" content="An overview of my self-management training"/>|]
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    $(widgetFile "self-management")

getSelfManagementLandingR :: Handler Html
getSelfManagementLandingR =
  defaultLayout $ do
    withTitle "Self-Management"
    toWidgetHead [hamlet|<meta name="description" content="An overview of my self-management training"/>|]
    $(widgetFile "self-management-landing")
