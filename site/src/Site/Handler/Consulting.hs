{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Consulting where

import Site.Foundation

getConsultingR :: Handler Html
getConsultingR = do
  defaultLayout $ do
    withTitle "Technical Consulting"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="An overview of my technical consulting services"/>|]
    $(widgetFile "consulting")