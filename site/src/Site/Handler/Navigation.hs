module Site.Handler.Navigation
  ( getPreviousR,
    getNextR,
  )
where

import qualified Data.Map.Strict as M
import Site.Foundation

getPreviousR :: Text -> Handler Html
getPreviousR url = do
  entries <- loadIO $ sortEntries . M.toList <$> getEntries
  let prevE = do
        ix <- findIndex ((== url) . fst) entries
        atMay entries $ ix - 1
  redirect $ maybe HomeR (PostsR . fst) prevE

getNextR :: Text -> Handler Html
getNextR url = do
  entries <- loadIO $ sortEntries . M.toList <$> getEntries
  let nextE = do
        ix <- findIndex ((== url) . fst) entries
        atMay entries $ ix + 1
  redirect $ maybe HomeR (PostsR . fst) nextE
