{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module Site.Handler.WebSocket (getWebSocketR) where

import Control.Concurrent
import Path
import Site.Foundation
import System.FSNotify as Notify
import Yesod.AutoReload

getWebSocketR :: Handler ()
getWebSocketR = getAutoReloadRWith $
  liftIO $ do
    sendRefreshVar <- newEmptyMVar -- A variable to block on
    Notify.withManager $ \mgr -> do
      let predicate e = case e of
            -- Don't watch removed events, in case the file is rewritten, so we don't get a 404 when reconecting
            Removed {} -> False
            _ ->
              let suffixes =
                    [ ".swp",
                      "~",
                      ".swx",
                      "4913" -- https://github.com/neovim/neovim/issues/3460
                    ] -- Editors make files like this, no need to refresh when they are written.
               in not $ any (`isSuffixOf` (eventPath e)) suffixes
          act e = putMVar sendRefreshVar e
      let dirs = ["content", "assets", "style", "logo"]
      forM_ dirs $ \d -> do
        ad <- resolveDir' d
        watchTree mgr (fromAbsDir ad) predicate act
      putStrLn "Waiting for a file to change."
      e <- takeMVar sendRefreshVar
      print e
