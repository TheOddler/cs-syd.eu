module Site.Handler.Key
    ( getKeyR
    ) where

import Site.Foundation

getKeyR :: Handler Html
getKeyR =
    redirect $ AssetsStaticR _0xBECF334611DF74C44746BF81F1260E2F09ABC023_txt
