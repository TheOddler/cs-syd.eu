{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Speaking where

import Site.Foundation

getSpeakingR :: Handler Html
getSpeakingR = do
  defaultLayout $ do
    withTitle "Speaking"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="An overview of my speaking services"/>|]
    $(widgetFile "speaking")

getSpeakingWorkingRemotelyR :: Handler Html
getSpeakingWorkingRemotelyR = do
  defaultLayout $ do
    withTitle "Talk: Working Remotely"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="Lecture: Working remotely"/>|]
    $(widgetFile "working-remotely")

getSpeakingEthicalDesignOnlineR :: Handler Html
getSpeakingEthicalDesignOnlineR = do
  defaultLayout $ do
    withTitle "Talk: Ethical Design Online"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="Lecture: Ethical design online"/>|]
    $(widgetFile "ethical-design-online")

getSpeakingSelfManagementWorkshopR :: Handler Html
getSpeakingSelfManagementWorkshopR = do
  defaultLayout $ do
    withTitle "Talk: Self-Management Workshop"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="Lecture: Self-management Workshop"/>|]
    $(widgetFile "self-management-workshop")

getSpeakingInternetSecurityR :: Handler Html
getSpeakingInternetSecurityR = do
  defaultLayout $ do
    withTitle "Talk: Internet Security"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="Lecture: Internet Security"/>|]
    $(widgetFile "internet-security")
