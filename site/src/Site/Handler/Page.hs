{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Page
  ( getCompanyR,
    getContactR,
    getCvsR,
    getHousingR,
    getNotesR,
    getP2pMentorshipR,
    getTalksR,
    getThesisR,
  )
where

import Site.Foundation

getCompanyR :: Handler Html
getCompanyR = pageHandler "company"

getContactR :: Handler Html
getContactR = pageHandler "contact"

getCvsR :: Handler Html
getCvsR = pageHandler "cv"

getHousingR :: Handler Html
getHousingR = pageHandler "housing"

getNotesR :: Handler Html
getNotesR = pageHandler "notes"

getP2pMentorshipR :: Handler Html
getP2pMentorshipR = pageHandler "p2p-mentorship"

getTalksR :: Handler Html
getTalksR = pageHandler "talks"

getThesisR :: Handler Html
getThesisR = pageHandler "thesis"

pageHandler :: Text -> Handler Html
pageHandler fn = do
  page <- getPage fn
  defaultLayout $ do
    withTitle $ mdTitle page
    toWidgetHead
      [hamlet|
        <meta name="description" content="#{pageDescription $ mdExtra page}"/>
      |]
    $(widgetFile "page")
