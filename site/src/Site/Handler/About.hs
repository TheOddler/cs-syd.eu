{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.About where

import Site.Foundation

getAboutR :: Handler Html
getAboutR = do
  es <- loadIO getEntries
  defaultLayout $ do
    withTitle "About"
    addScriptRemote "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"
    toWidgetHead [hamlet|<meta name="description" content="About Tom Sydney Kerckhove"/>|]
    $(widgetFile "about")
