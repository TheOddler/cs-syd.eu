{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.Handler.Rss
  ( getRssR,
  )
where

import qualified Data.Map.Strict as M
import Data.Time
import Site.Foundation
import Text.Blaze.Html as HTML
import Yesod.RssFeed

getRssR :: Handler RepRss
getRssR = do
  feed <- loadIO getFeed
  rssFeed feed

getFeed :: Load (Feed (Route Site))
getFeed = do
  entries <- getEntries
  pure $
    Feed
      { feedTitle = "CS SYD",
        feedLinkSelf = RssR,
        feedLinkHome = HomeR,
        feedAuthor = "Tom Sydney Kerckhove <syd@cs-syd.eu",
        feedDescription = "CS SYD Blog",
        feedLanguage = "en-gb",
        feedUpdated = maximum $ M.map (dayTime . entryLastUpdated) entries,
        feedLogo = Just (LogoR positive_logo_png, "CS SYD Logo"),
        feedEntries = flip map (M.toList entries) $ \(url, md@MdFile {..}) ->
          FeedEntry
            { feedEntryLink = case mdExtra of
                PostEntry _ -> PostsR url
                QuoteEntry _ -> QuoteR url,
              feedEntryUpdated = dayTime $ entryLastUpdated md,
              feedEntryTitle = mdTitle,
              feedEntryContent = HTML.preEscapedToHtml mdRendered,
              feedEntryEnclosure = Nothing,
              feedEntryCategories = []
            }
      }

entryLastUpdated :: Entry -> Day
entryLastUpdated e = fromMaybe (entryDate e) (mdLastUpdated e)

dayTime :: Day -> UTCTime
dayTime d = localTimeToUTC utc $ LocalTime d midnight
