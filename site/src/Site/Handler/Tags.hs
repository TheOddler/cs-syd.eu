{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Tags
  ( getTagsR,
  )
where

import Site.Foundation

getTagsR :: Handler Html
getTagsR = do
  tagSizesMap <- loadIO getTagSizesMap
  defaultLayout $ do
    withTitle "Tags"
    toWidgetHead [hamlet|<meta name="description" content="An overview of all tags for blog posts"/>|]
    $(widgetFile "tags")
