{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Drafts
  ( getDraftsR,
    getDraftR,
  )
where

import qualified Data.Map as M
import qualified Data.Text as T
import Site.Foundation

getDraftsR :: Handler Html
getDraftsR = do
  renderDrafts <- siteDrafts <$> getYesod
  unless renderDrafts notFound
  drafts <- loadIO $ sortDrafts . M.toList <$> getDrafts
  defaultLayout $ do
    withTitle "Drafts"
    toWidgetHead [hamlet| <meta name="description" content="Draft Posts"/> |]
    $(widgetFile "drafts")

getDraftR :: Text -> Handler Html
getDraftR url = do
  renderDrafts <- siteDrafts <$> getYesod
  unless renderDrafts notFound
  drafts <- loadIO getDrafts
  case M.lookup url drafts of
    Nothing -> notFound
    Just md ->
      defaultLayout $ do
        setTitle $ toHtml $ T.unwords ["CS SYD", "-", mdTitle md]
        -- Syntax highlighting
        addStylesheetRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/haskell.min.js"
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/nix.min.js"
        -- Maths
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
        toWidgetHead [hamlet|<meta name="description" content="#{draftDescription $ mdExtra md}"/>|]
        $(widgetFile "draft")
