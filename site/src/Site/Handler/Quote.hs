{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Quote
  ( getQuoteR,
  )
where

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import Site.Foundation

getQuoteR :: Text -> Handler Html
getQuoteR url = do
  quotes <- loadIO getQuotes
  entries <- loadIO getEntries
  case M.lookup url quotes of
    Nothing -> notFound
    Just md -> do
      let (mPrevE, mNextE) = findNextAndPrevious url fst $ sortEntries $ M.toList entries
      neverExpires
      defaultLayout $ do
        setTitle $
          toHtml $
            T.unwords
              [ "CS SYD",
                "-",
                mdTitle md
              ]
        toWidgetHead [hamlet|<meta name="description" content="#{quoteDescription $ mdExtra md}"/>|]
        $(widgetFile "quote")
