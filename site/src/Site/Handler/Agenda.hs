{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Agenda where

import Site.Foundation

getAgendaR :: Handler Html
getAgendaR = do
  defaultLayout $ do
    withTitle "Agenda"
    toWidgetHead [hamlet|<meta name="description" content="Instructions for making an appointment."/>|]
    $(widgetFile "agenda")
