{-# LANGUAGE OverloadedStrings #-}

module Site.Handler.Cv
    ( getCvR
    ) where

import qualified Data.Text as T

import Site.Foundation

-- TODO get rid of these and use type-safe redirects in post files
getCvR :: [Text] -> Handler Html
getCvR t = do
    neverExpires
    redirect $ T.intercalate "/" $ "/cv-static/res" : t
