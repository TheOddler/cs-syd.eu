{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Support
  ( getSupportR,
    getThankyouR,
  )
where

import qualified Data.FileEmbed as FileEmbed
import Site.Foundation

getSupportR :: Handler Html
getSupportR =
  defaultLayout $ do
    withTitle "Support"
    addScriptRemote "https://c6.patreon.com/becomePatronButton.bundle.js"
    addScriptRemote "https://buttons.github.io/buttons.js"
    addScriptRemote "https://platform.linkedin.com/in.js"
    toWidgetHead [hamlet| <meta name="description" content="Support CS SYD"/> |]
    $(widgetFile "support")

mailinglistWidget :: Widget
mailinglistWidget =
  toWidget $
    preEscapedToMarkup
      ($(FileEmbed.embedStringFile "templates/mail-signup-form.html") :: Text)

getThankyouR :: Handler Html
getThankyouR =
  defaultLayout $ do
    withTitle "Thank You!"
    $(widgetFile "thankyou")
