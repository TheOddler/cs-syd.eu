{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Home
  ( getHomeR,
  )
where

import qualified Data.Map.Strict as M
import Site.Foundation

getHomeR :: Handler Html
getHomeR = do
  mVerificationTag <- siteVerification <$> getYesod
  mPinterestVerificationTag <- sitePinterestVerification <$> getYesod
  es <- loadIO $ sortEntries . M.toList <$> getEntries
  defaultLayout $ do
    setTitle "CS SYD"
    forM_ mVerificationTag $ \verificationTag ->
      toWidgetHead
        [hamlet|<meta name="google-site-verification" content="#{verificationTag}"/>|]
    forM_ mPinterestVerificationTag $ \pinterestVerificationTag ->
      toWidgetHead
        [hamlet|<meta name="p:domain_verify" content="#{pinterestVerificationTag}"/>|]
    toWidgetHead [hamlet| <meta name="description" content="CS SYD: Technical Services, Self-management Training and Blog"/> |]
    $(widgetFile "home")
