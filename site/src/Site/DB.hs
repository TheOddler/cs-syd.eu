{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Site.DB where

import Data.Int
import Data.Text (Text)
import Database.Persist.Sqlite
import Database.Persist.TH
import GHC.Generics (Generic)

share
  [mkPersist sqlSettings, mkMigrate "siteMigration"]
  [persistLowerCase|

TwitterStatus
  status Int64
  entry Text
  UniqueTwitterStatus entry

RedditPost
  subreddit Text
  post Text
  entry Text
  UniqueRedditPost subreddit entry
  deriving Show
  deriving Eq
  deriving Generic
|]
