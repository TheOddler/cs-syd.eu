module Site.Logo.TH where

import Language.Haskell.TH
import Path
import Path.IO
import Site.Constants
import System.Environment
import System.Exit
import Yesod.EmbeddedStatic

mkEmbeddedLogo :: Q [Dec]
mkEmbeddedLogo = do
  md <- runIO $ lookupEnv "LOGO_DIR"
  nl <- runIO $ lookupEnv "NO_LOGO"
  let doEmbed d =
        [ embedDirAt "positive" (d <> "/positive"),
          embedDirAt "negative" (d <> "/negative"),
          embedFileAt "favicon.ico" (d <> "/favicon/favicon.ico")
        ]
  fs <- case md of
    Nothing -> case nl of
      Nothing -> runIO $ die "No logo found."
      Just _ -> do
        d <- resolveDir' "logo"
        runIO $ putStrLn $ unwords ["WARNING: Including logo files from submodule at path: ", fromAbsDir d]

        pure $ doEmbed $ fromAbsDir d
    Just d -> do
      runIO $ putStrLn $ unwords ["Including logo in dir: ", d]
      pure $ doEmbed d
  mkEmbeddedStatic development "staticLogo" fs
