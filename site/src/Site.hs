module Site where

import Site.GeneratePasswordHash
import Site.OptParse
import Site.Serve


main :: IO ()
main = do
  Instructions disp Settings <- getInstructions
  case disp of
    DispatchServe serveSets -> serve serveSets
    DispatchGeneratePasswordHash pw -> generatePasswordHash pw
