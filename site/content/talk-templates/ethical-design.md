---
title: Ethical Design of User Interfaces
category: Popular
audience: non-technical users of technology
description: Technology companies hijack your mind and influence your behaviour. This talk goes into how that happens and how we can mitigate and/or prevent the resulting damage.
---

Technology companies hijack your mind and influence your behaviour.
This talk goes into how that happens and how we can mitigate and/or prevent the resulting damage.

### Past occurrences

- Ethisch Ontwerp van Gebruikerservaringen [Slides](https://docs.google.com/presentation/d/1RHRNl9SYe5mi27KrMzoF__8W2qYNm3oAJUpTtu-PZKs/edit?usp=sharing) @ Marnix Ring Kempenland 2018-05-17

