---
title: Testing for beginners
category: Technical
audience: Beginner Haskell Programmers
description: The talks will form a complete introduction to testing in Haskell.
---

The talks will form a complete introduction to testing in Haskell.
They will cover the different kinds of tests, and how to write them in Haskell, best practices and generators and their best practices.
It will be a combination of lectures and hands-on workshops. 

### Past occurrences

- Monadic Party, Part 1 & 2, 2018-06-13 [Video](https://www.youtube.com/watch?v=S3BRNsbk6Ks)
  <iframe width="854" height="480" src="https://www.youtube.com/embed/S3BRNsbk6Ks" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
- Monadic Party, Part 3 & 4, 2018-06-14 [Video](https://www.youtube.com/watch?v=qre77C9rve4)
  <iframe width="854" height="480" src="https://www.youtube.com/embed/qre77C9rve4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
- Monadic Party, Part 5 & 6, 2018-06-15 [Video](https://www.youtube.com/watch?v=ht25V9Ty2DE)
  <iframe width="854" height="480" src="https://www.youtube.com/embed/ht25V9Ty2DE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


