---
title: Safe Software in Industry
category: Technical
audience: Computer Science Students
description: We explore the trade-offs that show up in industrial software engineering, in particular with respect to software that has a safety requirement, and discuss the technical considerations and conclusions that follow. As it happens, using Haskell is frequently one of those results, so we will also dive into examples where using Haskell and Haskell-like languages were the chosen option. Examples include work at Spam fighting at Facebook, Design of cities at ETH Zuerich, crypto currency at the Cardano Foundation, etc.
---

We explore the trade-offs that show up in industrial software
engineering, in particular with respect to software that has a safety
requirement, and discuss the technical considerations and conclusions
that follow. As it happens, using Haskell is frequently one of those
results, so we will also dive into examples where using Haskell and
Haskell-like languages were the chosen option. Examples include work at
Spam fighting at Facebook, Design of cities at ETH Zuerich, crypto
currency at the Cardano Foundation, etc.


### Bio

The speaker is an experienced safety engineer with a background in
testing and property testing. His past work includes property discovery,
validity and validity-based testing. He currently works to prevent
dangerous software from doing damage.

### Past occurrences

- [Safe Software in industry @ KU Leuven 2018-02-19](https://dtai.cs.kuleuven.be/seminars/practical-safe-software-haskell-industry-tom-sydney-kerckhove)
