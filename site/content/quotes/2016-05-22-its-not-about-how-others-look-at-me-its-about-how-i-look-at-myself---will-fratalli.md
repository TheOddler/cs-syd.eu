---
layout: quote
quote: It's not about how others look at me. It's about how I look at myself.
author: Will Fratalli
title: It's not about how others look at me. It's about how I look at myself. - Will Fratalli
tags: beastly, suits, blind
---

I like to dress up.
For some reason my clothing has prompted many strong opinions on the matter.
In this post I'd like to put some misconceptions to rest.

<div></div><!--more-->

### Suits

Will Fratalli is a fictional character.
He's a blind tutor to a recently deformed kid that is obsessed with his appearance.
This character manages to voice my opinion on the matter perfectly.

I don't care about what people think about how I look.
I do, however, care about what I think about how I look.

I am fortunate enough to be able to see and fortunate enough to have a choice in the matter of what I get to wear.
I am young and lucky to be healthy.
This is the perfect time to enjoy being able to dress up however I want.

I love to wear suits, so I wear suits.
*That is all.*

<!--
I wore suits to school.
I wear suits to university, both as a student and as a teaching assistant.
I even wore suits at Google.

People have really strong opinions on my choice of clothing.
I have been yelled at, insulted and ridiculed for wearing a suit, but I've also been complimented and even had someone told me he was inspired by it.
I've been told I aspire to be like [a certain (fictional!) womanizer](https://en.wikipedia.org/wiki/Barney_Stinson), that I am obsessed with appearances, etc..
Often I am the only one in a situation to look formal.
At Google someone even called security because I looked like I didn't belong there.
-->


### Misconceptions

- Doesn't it make formal occasions less special?

Maybe, but why should only formal occasions be special?
For me, every day is special.


- Aren't suits really uncomfortable?

No, they're like a sweat suit over your entire body, but one that looks really good.


- So, you _always_ wear suits?

No, of course not.


- You look like you consider yourself superior to others.

That's really not my problem.
Again: I don't care what you think about it.


- Do you aspire to be like [a certain (fictional!) womanizer](https://en.wikipedia.org/wiki/Barney_Stinson)?

No, that's ridiculous.


