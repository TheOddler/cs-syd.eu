---
layout: quote
author: Benjamin Franklin
quote: We were all huddled together, as wet as water could make us.
title: We were all huddled together, as wet as water could make us. - Benjamin Franklin
tags: optimism, beauty
---

I stumbled upon this wonderful quote while reading the autobiography of Benjamin Franklin.
Allow me, for a moment, to write a little fuzzily.

I think this really is one of the more optimistic quotes I have ever read.
Getting wet from rain can be a little uncomfortable at times, but really, as long as you can huddle together, it can make for an amazing moment.

"As wet as water could make us" is a beautiful way of saying that they were really wet, but that that wasn't so bad.
Even more fuzzily: Really, water can only get you so wet.
There are way worse things in life and I find this an eloquent way of phrasing that.
