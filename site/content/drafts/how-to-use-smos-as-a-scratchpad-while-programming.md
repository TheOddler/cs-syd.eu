---
title: How to use Smos as a scratchpad while programming
tags: self-management, smos, programming
---

Jumping into a codebase to make some fixes can be more stressful than necessary, because you usually find more things to fix than you you initially set out to.
You know it would be better to separate the fixes into independent changes.
That means that you are now faced with the choice between stressing out about remembering all the fixes you want to make, not making any of the other fixes, and just doing everything at once.
You can use Smos to solve this problem by using it as a scratchpad while diving into a project.

<div></div><!--more-->

### An example dive

As a running example, we will use a situation that you may be familiar with: Someone opens an issue for a bug on an old-ish open-source project that you've made.
Since the last time you have touched this project, you have learnt a lot.
Nowadays you would do things so much better.
