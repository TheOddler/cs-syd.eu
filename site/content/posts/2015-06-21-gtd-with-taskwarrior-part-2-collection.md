---
layout: post
title: "GTD with Taskwarrior, Part 2: Collection"
tags: Taskwarrior, productivity, work, GTD, tutorial, CLI, terminal, collection
---

In the second part of this series on how to use Taskwarrior to implement GTD, I will show you the first step of GTD: collection.

<div></div><!--more-->

Collection allows you to capture 'stuff' and thereby freeing up your mind to do what it's good at: creative thinking.


### Collection requirements
Let's look at the requirements of GTD's collection:

- The inbox is where you capture ideas and tasks as they occur to you.
- The transaction cost of putting something in your inbox should be as low as possible.
- If your inbox is not at zero, it has to be immediately visible.
- You should need as few inboxes as possible.


### Taskwarrior inbox

An item in your inbox is really just a "this-needs-to-be-processed-item".
As such, it is something that you need to *do* with the added semantics that it's just a *decision*.
There is no reason why Taskwarrior can't also be *your inbox* given that it's used correctly.

You can't just add inbox-items to Taskwarrior just like you would add normal tasks.
That would ruin the concept of an inbox as well as the rest of Taskwarrior.
This needs to be handled with a little more care.


#### Adding 'stuff'
We need to make it *very* easy to add an item to our inbox.
I do this with an alias.
The following line is in my aliases file.

``` bash
alias in='task add +in'
```

Note that the `+in` tag is used to add the semantics of an inbox item to the task.

Now, when you want to add an inbox item, you just type `in`:

```
$ in Set up meeting with Dave
```

That's two letters instead of twelve.
You could bring it down to one, of course.


#### Ever-present stuff

We can now make use of the `+in` tag, by making sure that the `+in` tasks show up exactly when we need them to.

You need a way to see how may tasks are in you inbox.
There are two ways to do this.
You can use Taskwarrior's built-in urgency system to have `in` tasks show up in your `next` report, or you can have the number of tasks in your inbox show up in you terminal's prompt.


##### Urgency

To add urgency to an `in` task, we modify its urgency coefficient.
You can do this by putting the following line in your `~/.taskrc` file:

```
urgency.user.tag.in.coefficient=15.0
```

I usually recommend against this because it will clutter your `next` report later.


##### Inbox in terminal's prompt
To add a the number of inbox items to your terminal's prompt, you can modify the `PS1` environment variable.
You can add this to your shell's '.rc' file:

``` bash
export PS1='$(task +in +PENDING count) '$PS1
```

Mind the *single* quotes.
Now your shell's prompt will say how many tasks are in your inbox.


#### Report
Later, when you will process your inbox, you will need to see it.
I have made a custom report to view my inbox.
You can configure it by putting the following lines in you `~/.taskrc` file.

```
report.in.columns           = id,description
report.in.description       = Inbox
report.in.filter            = status:pending limit:page (+in)
report.in.labels            = ID,Description
```

To see this report, just type `task in`.
It is a very minimal report, perfect for processing.

We will go into processing your inbox in later parts.
For now, just try to adopt the habit of using your fancy new inbox to dump all your mental "stuff".

