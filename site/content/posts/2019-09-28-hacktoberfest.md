---
title: Hacktoberfest
tags: Haskell, cursors, smos, programming, hacktoberfest
---

CS SYD is participating in [Hacktoberfest](https://hacktoberfest.digitalocean.com/) this year.
Look here to find some issues to get started with.

<div></div><!--more-->


Contributions are very welcome, and do not need to be pre-defined, but below are some good issues to get started with.
Documentation in particular is very welcome, and can be a great way to get started with any given project.

If you want to learn more about testing, property testing, GTD, distributed systems or all-round real-world haskell programming, this could be a great opportunity!

The following repositories have some [Hacktoberfest](https://hacktoberfest.digitalocean.com/) issues:

- [Validity](https://github.com/NorfairKing/validity):  Validity-based testing
  - [Instances for all vectors](https://github.com/NorfairKing/validity/issues/68)

- [Cursor](https://github.com/NorfairKing/cursor): Editor-agnostic zippers with a focus on better testing
  - [Make `aboveNext` and `abovePrev` symmetric](https://github.com/NorfairKing/cursor/issues/10)
  - [Make promote and demote symmetric](https://github.com/NorfairKing/cursor/issues/9)
  - [A counter cursor for integers](https://github.com/NorfairKing/cursor/issues/8)

- [Smos](https://github.com/NorfairKing/smos): A purely functional semantic forest-based editor for Getting Things Done. 
  - [Commands and keybinding documention](https://github.com/NorfairKing/smos/issues/16)
  - [`timestampSelect` puts cursor in the wrong place](https://github.com/NorfairKing/smos/issues/13)
  - [Copy-pasting a node/tree](https://github.com/NorfairKing/smos/issues/17)
  - [Clocking out should un-collapse every entry on the path to the roots](https://github.com/NorfairKing/smos/issues/18)

- [Hastory](https://github.com/NorfairKing/hastory): Advanced Applied Laziness for terminal-use optimisation
  - [Use Sqlite instead of json](https://github.com/NorfairKing/hastory/issues/6)
  - [A simple append-only sync server](https://github.com/NorfairKing/hastory/issues/7)
  

You are always welcome to email me for advice or with questions.
I will be available to help, coach, guide and review.

Happy Hacking!
