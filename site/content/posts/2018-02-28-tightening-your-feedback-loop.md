---
title: Tightening your feedback loop
tags: automation, speed, effectivity, development
---

Programming usually does not happen by writing a program from start to finish, deploying it, and then never touching it again because it is done.
<div></div><!--more-->
Instead, programming happens in incremental cycles:

- Notice space for improvement.
- Make changes.
- Get feedback on those changes.
- Repeat.

I will call these cycles the feedback loop.

Noticing space for improvement is rarely a difficult or lengthy task.
Making the required changes to achieve this improvement is the part of the loop that is most frequently optimised.
We go to school, gather experience, we use special editors for code, etc...

However, the part of this loop that is most often neglected, is the part where we get feedback on our changes.
In this post I would like to approach optimising this part of the feedback loop.

### Step 1: Have a feedback loop

In other words: "Make sure you are getting feedback."
Without getting any feedback on the changes you make, there is no way to tell whether they have achieved their goal.
Work will progress slowly and it will feel useless and mindnumbing.

The first step is to establish a feedback loop.
This includes setting up the infrastructure to run that feedback loop.
At this point it does not matter yet how quick you get your feedback.
Just make sure you are getting feedback regularly.
This should be the first step you take in any project, and for every change.

One of the the more crude forms of feedback is to run the changed code, and see whether it does what you were going for.
This seems obvious, but it requires that you have set up the means to run your code, which is nontrivial in some cases.

### Step 2: Tighten your feedback loop

Once you have established a feedback loop, it is time to make it as short as possible, on average.
Sometimes the only way to get the feedback you need, is to run your code and have a look.
However, it is usually possible to get feedback sooner.
In this way, if you get bad feedback, you will be able to react quicker.
The quicker you get feedback, the less time is spent waiting, or, even worse, backtracking your changes.

A more refined way to get feedback is to test your code using code tests instead of by running it manually and looking at the result.
Never running 'main' is a great way to speed up your feedback loop.

A lot of programming languages stop here.
However, a good way to speed up your feedback loop even further is to use a language that can give you feedback at compile-time.
Compiled languages, specifically languages with a picky compiler, can give you feedback much sooner than you can run your tests.

At this point your feedback loop should look something like this:

1. Make a change
2. Run the compiler
3. If the compiler complains, go to step 1.
4. Run the tests
5. Evaluate whether your change is satisfactory from the output that you get.
6. Repeat

### Step 3: Automate your feedback loop

To speed up your feedback loop even further, we can automate some of the steps that are currently being done manually.
Whenever you make a change, your operating system notices this.
You can use this fact to start the next step in the process immediately.

Tools like [Steel Overseer](https://github.com/schell/steeloverseer.git) allow you to set up commands to run on every file change.
This means that it is no longer necessary to manually press the button to compile/test your code.
You could set up a feedback loop that goes even quicker:

1. Make a change
2. The compiler is run automatically.
3. If the compiler complains, go to step 1.
4. If the compiler doesn't complain, the tests are run automatically.
4. Evaluate whether your change is satisfactory from the output that you get.
5. Repeat

