---
layout: post
title: Return to taskwarrior
tags: Taskwarrior,org mode,productivity,GTD,zsh
---

Last year I wrote a few articles on [how](http://cs-syd.eu/posts/2015-06-14-gtd-with-taskwarrior-part-1-intro.html) [to](http://cs-syd.eu/posts/2015-06-21-gtd-with-taskwarrior-part-2-collection.html) [use](http://cs-syd.eu/posts/2015-06-28-gtd-with-taskwarrior-part-3-tickling.html) [Taskwarrior](http://taskwarrior.org/) [to](http://cs-syd.eu/posts/2015-07-05-gtd-with-taskwarrior-part-4-processing.html) [implement](http://cs-syd.eu/posts/2015-07-12-gtd-with-taskwarrior-part-5-doing.html) [GTD](http://gettingthingsdone.com/).
Shortly after, I went back to using [Emacs](https://www.gnu.org/software/emacs/) [Org mode](http://orgmode.org/) instead.
Now I'm back to using Taskwarrior and I'm using it entirely differently this time.

<div></div><!--more-->


### Moving away from Taskwarrior

To my surprise, I didn't move away from Taskwarrior because I was making things too complicated.
I moved away from Taskwarrior because I wanted everything productivity-related in one place.
Org mode handles tracking progress/history on and keeping notes for projects much better than Taskwarrior does.
That is, if you use Taskwarrior the naive way.

Of course I missed the fact that Taskwarrior is much more easily programatically controlled as I was trying to automate.


### Bad practices

As it turns out, removing a lot of bad practices from my workflow made it much easier to go back to Taskwarrior.


#### Too much stuff

Part of GTD is to keep _everything_ safe in you system, so you can trust the system.
I was keeping all notes on a project in a single file `project.org`.
There was no distinction between the tasks of the project I was committed to doing at the moment and the tasks I wanted to do eventually.
For regular projects this doesn't make sense at all, but for software projects this is the natural way to do it.


#### Waiting

The next problem was that event hough I had a way to construct a waiting-for list, I neglected to keep it up to date.
None of the items on the list reminded me of the list enough to check them off once the waiting was over.
The items on the waiting-for list were distributed across multiple `project.org` files.
I would have to manually go over the waiting-for list every week to keep it up to date.
This made I/O bound projects unmanageable.

### Moving back to Taskwarrior

#### Someday-maybe and Notes

To move back so Taskwarrior, I realized I was going to need to keep my notes per project.
For starters, I wrote this tiny zsh script:

``` zsh
note_ () {
  local id="$1"
  local dir="$HOME/workflow/projects"
  local file="$dir/$id.org"

  mkdir -p $dir
  e "$file"
}

alias n=note_
```

Now I can type `n project` to bring up the notes for any project.
This way I can keep notes on every project and still use Taskwarrior.

The most important change is that now I only keep my actions on taskwarrior and keep all the someday-maybe actions for a specific project in the `project.org` file.
Keeping the amount of tasks in Taskwarrior small is essential to being able to manage them.
It's good to keep every (eventual) task in the system, but put them in the someday-maybe list, not in your regular task list.

#### Automatically up to date projects

Now I could take advantage of the fact that taskwarrior can be accessed programatically.
Here is a little script that tells me what projects have no `+next` or `+waiting` action.

``` zsh
fun () {
    cat << EOF
import tasklib
tw = tasklib.TaskWarrior()
result = set(tw.execute_command(["+PROJECT", "+PENDING", "+READY", "-waiting", "_projects"]))
  - set(tw.execute_command(["+PENDING", "+next", "_projects"]))
for i in result:
    print(i)
EOF
}

python -c "$(fun)"
```

Now I just added the following to my `.zshrc` so that I would be reminded to keep my projects up to date with `+next` actions whenever I open a terminal.

``` zsh
projects=$(projects_without_next_action.sh)
if [ "$projects" != "" ]
then
  print_colored_text RED "Attention: The following projects don't currently have a next action:\n"
  echo $projects
  echo
fi
```

This ensures that my system reminds me to keep my projects up to date.

#### Automatically polling the waiting-for items

To finish the transition back to Taskwarrior, I added another piece of code to my `.zshrc` to ensure that I keep my `+waiting` items up to date:

``` zsh
waiting=$(task +waiting +PENDING count)
if [ "$waiting" != "0" ]
then
  echo "Any progress on these waiting-fors?"
  task +waiting +PENDING ls
fi
```

Now my system encourages me to keep my `+waiting` items up to date as well.


### Best practices for Taskwarrior

With my projects and `+waiting` items automatically up to date, I could now safely transition back to Taskwarrior.

In conclusion, using Taskwarrior the naive way has some huge disadvantages, but if you adhere to these best practices it is actually better than org mode.

- Commit to fewer tasks, but record the others in a someday-maybe list.
- Use the someday-maybe list heavily. Look at them every week in the review to make sure you're okay with not doing what's on the list.
- Commit to as few projects at a time as possible and keep them up to date.
- Use `+waiting` items to keep tabs on I/O bound projects.

