---
layout: post
title: "The hidden 'at least'"
tags: frustrations, table flipping, human language
---

Yet again have I had an aggravating discussion about the specification of an abstract idea.
The discussion involved a simple math problem.
The specification of this problem is a little long to be practical for this post, so I will simplify the description without loss of generality.

> John owns five apples.

Anticlimactic, right?
How could anyone have a discussion about this simple sentence?

<div></div><!--more-->

#### Let's get started
One of the principles of solving a math problem is to assume that what is given, is true.
It might not be true that John owns five apples, but that is not relevant to the case.
We will assume that it is true, and look at what we can learn from that.

The next assumption is called the law of excluded middle, which simply means that a statement is either true or false.
There is nothing in between.
This means that an apple is either owned by John, or not owned by John.
Note that when we don't know whether something is true or false, there is still no third option.

We will assume that apples are indivisible, and that there are no negative apples.
Let $x$ be the exact amount of apples that John owns.
This means that $x$ is a whole number greater than zero.

From here on, we will find a range of values for $x$ so that the original statement still holds.
Are you still with me up to this point...?
Yes? Good.

#### Some questions
- Question: Does John own an apple?

  Answer: Yes. John even owns more than one apple.

- Question: Does John own three apples?

  Answer: Yes. John even owns more than three apples.
  If you disagree here, imagine that John has to pay three apples.
  Would he be able to do so? Yes, exactly.

- Question: Does John own six apples?

  Think of your own answer.
  If your answer is "No, John owns five apples, not six", then imagine, for a minute, that John does own six apples.
  Would you say that John owns five apples, now? Yes, exactly. If John owned six apples, you could also say that he owned five apples.
  This means that we really don't know whether John owns six apples or not.

  Answer: Maybe.

Are you still with me?
Hang in there!

#### The interval
Back to $x$.
If $x$ is strictly smaller than five, then the statement "John owns five apples" is false.
If $x$ is equal to five, the statement is true.
And now for the interesting part: If $x$ is greater than five, the statement is .... _true_, exactly!
We can now write down an interval for $x$:

$$ x \in [5,\infty[ $$

There is a hidden 'at least' in the original specification.
If it were more explicitly clear, then it would say something like this:

>   John owns at least five apples.

The amount of information in this new statement is exactly the same as the amount of information in the original statement. That is to say, in every case where this statement is true, the original statement is also true, and vice versa.

$x$ does not simply equal $5$.
If we wanted to specify that John owned _exactly_ five apples, no more and no less, then we would have to rewrite the problem specification:

>   John owns exactly five apples.

The amount of information is this statement is greater than the amount of information in the original statement, so we can't just replace the original one by this statement.

Are you still following along? Great!

#### Frustrations
You might agree with me here, and I would congratulate you, but you might as well have flipped a table somewhere along this post.
The entire point of this post is to show that you have to be really careful with the information that you have, and be sure not to add any extra information.

I have to agree that in reality, there might be extra information in the context of the situation.
Saying "John owns five apples" might mean something different.
The context may imply that this statement means that there are zombies coming, but then extra information is required to 'encode' that statement.

I have flipped many a table myself trying to discuss this subject.
Of course that's one of the reasons that I wrote this post.
I sincerely hope that this post aids me in decreasing the frequency of table flipping on my part.

