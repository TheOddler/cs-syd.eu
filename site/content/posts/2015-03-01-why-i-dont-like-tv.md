---
layout: post
title: Why I don't like TV
tags: TV
---

In [my post about time management](/posts/2015-02-22-what-do-we-do-with-our-time.html) I mentioned that I don't watch TV anymore.
People have asked my why a couple of times now.
I thought I'd answer their question here.

I don't watch TV anymore though the house I live in still contains a TV set.
I used to watch TV for hours every day.
Realising that watching TV wasn't helping me in any way, I stopped altogether.
I only started realising the flaws of TV afterwards.

<div></div><!--more-->

### Time warp

After months of not watching TV, I happened to end up watching a program while waiting at a friends place.
I noticed something about TV programs that I had never noticed when watching TV was a regular activity.

TV programs compress a large amount of events into a small amount of time so as to give you summarized, interesting content.
However, because the content is so compressed, it is not nearly an accurate representative of the actual events.
It gives you a skewed idea of what it's like to witness these events.
If you're watching TV a lot, you don't notice this.
You watch TV and feel like you're involved in what you're watching, especially if you're watching something that doesn't happen in your daily life.

Real life often requires a kind of patience and endurance that you seize to expect if you watch TV a lot.
This 'miscommunication' can bring great disappointment to anyone that wants to achieve anything.
Of course, this doesn't present a problem to anyone who hasn't grown up with TV and is aware of this.

The most extreme example I saw, was a show that featured a man who started bodybuilding and took part in a competition eight months later.
As a viewer, you might start to identify with what you see and think that you could easily do it as well.
However, those eight months were compressed down to 40 minutes.
The viewer gets a wrong idea of what it takes and will be disappointed by the difficulty if he ever decides to try it for himself.

This example is, of course, more extreme than others, but even regular content can miscommunicate this.

### A fixed schedule

Television is old.
It comes from a time that there wasn't much content to be broadcast.
In order for people to know when to watch and what to expect, programs were scheduled.

Of course, nowadays, there is so much content that scheduling programs to appear at a fixed time doesn't make sense anymore.
Moreover, there are other media that allow you to find the content you want, when you want it.
(An obvious examples here is youtube.)
Lives have changed as well, we know what we want to watch and we want to see it when it is convenient to us, rather than the producer.

### Advertisements

You pay for a television subscription, and then you still have to watch [increasingly long advertisements](https://www.youtube.com/watch?v=bFEoMO0pc7k) that break up you favorite shows.
There are better media available, where you get to choose whether to waste time on adds.
Online you get to choose when you watch advertisements through, for example, adblockers and/or subscriptions.

