---
layout: post
title: "A sleep experiment: The setup"
tags: sleep, experiment
---

I like to experiment with sleep behaviours.
As [I've said before](/posts/2014-11-16-how-to-get-up-early.html), I think monophasic sleep is the [electric meat](http://matt.might.net/articles/electric-meat/) of sleep behaviors.

I'm in the middle of studying for exams as I'm writing this.
This means that I'm in a very particular situation regarding social responsibilities.
I don't have to be anywhere regularly and I can (almost) ensure that I'm not disturbed when I'm studying in my room.

This is the perfect time to experiment with a new sleep behaviour.

<div></div><!--more-->

#### A little background.
Most people sleep monophasicly.
That means they sleep in one continuous block of time.
The average sleeper sleeps around eight hours a day.

![A diagram of a normal sleep schedule](/assets/sleep-experiment/normal.png)

Now, I personally don't know a lot about sleep from a research point of view, but it seems that even researchers aren't sure of much.

> “As far as I know, the only reason we need to sleep that is really, really solid is because we get sleepy.” - William Dement, retired dean of sleep studies, a co-discoverer of REM sleep, and co-founder of the Stanford Sleep Medicine Center after 50 years of research

We're told that you need eight hours of sleep.
While I object to such an arbitrary number, that is also an average, this is said about monophasic sleep.
Polyphasic sleep isn't even considered.

Polyphasic sleep is supposed to be more 'natural'.
That is, if you leave humans without artificial light, they will naturally start sleeping in two (or more) phases.

Polyphasic sleep allows you to rest quicker after 'energy consumption' so that you are rested more fully on average.
This also means that you don't need quite as much sleep and you'll feel more energized throughout the day.

#### Why we don't sleep polyphasically
In short, we don't sleep polyphasically because we don't sleep polyphasically.

The problem with polyphasic sleep is that your sleep schedule has to be more rigid.
You can't skip sleep phases and you can't postpone them as long as you can with the one phase.

Our social system is not prepared for this.
When was the last time you've had time to sleep for at least 90 minutes in the middle of the day?
Most of us are 'stuck' at work or school without a bed or time to sleep.

There are places where people do sleep polyphasically, for example around the Mediterranean sea.
People get up earlier and go to bed later, but they also sleep in the afternoon.
Granted, climate plays its role in this situation as well, but that's not something we can change.
These situations are a prime example of how the social system shapes when we sleep.

#### The 'experiment'
As I'm studying for exams, I have decided to do a personal polyphasic sleep 'experiment'.
This will _not_ be representative for the average human and it will be highly unscientific.
It is more a summary of my experience than an actual experiment.
It will, however, be something fun to try out and beneficial if it works out for me.

<small>
Please note that I am a somewhat experienced polyphasic sleeper.
This is a rather advanced polyphasic sleep behaviour and sleep it is not something to take lightly.
Do not try this yourself if you've only ever slept monophasicly.
If you would like to try polyphasic sleep, I'd recommend a biphasic sleep schedule.
</small>

![A diagram of my experimental sleep schedule](/assets/sleep-experiment/study.png)

The plan is to sleep for an hour and a half every six hours.
For lack of a better term, I've started calling it the study-sleeping schedule.

As you can see, my 'day' will be divided into four quarter days.
I will call them 'qdays' and I'll call the sleep phases 'qnights'.

At first, this seems like I'm wasting a lot of time sleeping, but keep in mind that this is two hours less than the average sleeper.
It also immediately becomes apparent why most people don't do this (apart from the fact that they're not used to it).
If I had a job or class, I would not be able to do this.

I'll have two extra hours to spend awake each day.
On top of that, I will be able to go to bed sooner after studying so that I can recover energy quicker as needed.

I expect that, at first, I will not be able to sleep during the first few qnights.
It will be important to make an effort to stay in bed anyway.
I also expect that I will try to sleep too long in some qnights.
It will be equally important to get up at the right time.

Let's get started!

