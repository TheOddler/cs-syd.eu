---
title: Property discovery at Haskell eXchange 2017
tags: easyspec, property discovery, Haskell
---

I am very pleased to announce that I will be a speaker at 2017's Haskell eXchange.
I will be speaking about property discovery and my involvement in the progress on this topic.

<div></div><!--more-->

My thesis has officially been completed and submitted.
I have made progress on the topic of functional property discovery and I look forward to sharing this progress.

My aim is to show that property discovery has the potential to revolutionise the way we write software in the medium-term future.
It would be great to spark interest to contribute to this concept and to move it further toward practical applicability.

### Why property discovery ?

Testing has been around for a while in practical software engineering.
However, it is still too often beneficial from a business perspective to omit any effort toward software correctness, in order to deliver more quickly.
Another problem with testing is that a programmer already has to know what it means for their code to be correct, before they can write the corresponding test cases.

The problem of having to come up with test cases has already been solved by property testing. QuickCheck is an implementation of this concept in Haskell and it is already used by Haskell programmers.
However, property testing has the nasty disadvantage that it exacerbates the problem of cost. 
Writing property tests is more difficult that writing unit tests, and even fewer programmers take the time to do so.
As a result, property testing is much more rarely used in practice than unit tests.

Property discovery has the potential to solve this last problem with property testing, by freeing the programmer from having to figure out what their code is supposed to do.
Property discovery consists of discovering properties of subject code, and subsequently generating the test code to test that these properties hold.
As such, property discovery has the potential to make it cheaper to write tested code than to write untested code, even in the short term. 

### My involvement

A proof of concept tool for Property discovery has already been written for Haskell. It is called [QuickSpec](https://github.com/nick8325/quickspec) and it is great at what it does.
However, QuickSpec has certain limitations that make it impractical for real-world use: It has a $O(N^{14})$ time complexity with respect to the size of your code base, and it requires a programmer to write code to invoke the property discovery.

For my master thesis, I contributed a tool called [`easyspec`](https://github.com/NorfairKing/easyspec).
This tool is a proof of concept implementation of a new approach called _signature inference_ that solves both of these problems with QuickSpec by automating the process of invoking the property discovery mechanism, and by cleverly choosing the inputs such that the runtime remains practical ($O(1)$ or $(O(N)$, depending on the use-case).

### My talk

In my talk, I will introduce Property testing and Property discovery from scratch.
No familiarity with testing or testing in Haskell is assumed, but familiarity with Haskell will come in handy to understand the examples.

The talk will be most enjoyable for anyone interested in practical approaches to software correctness.
It should also be a great example of why functional programming is the way forward on the path to correct software.
