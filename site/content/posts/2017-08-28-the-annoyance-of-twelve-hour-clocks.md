---
title: The annoyance of twelve hour clocks
tags: clock, time, annoyance
---

Something about the way we speak about time has always bothered me.
Specifically, using twelve hour clocks, AM and PM.

First of all, the purpose of a system of measurement is to ease measurements and reporting.
Twelve-hour time has failed that purpose.

<div></div><!--more-->

### The workings of twelve hour time

In twelve-hour time, a day is divided up into two pieces of 12 hours.
One half is called AM and the other half PM.
The hours take values from 1 to 12.
Midnight is at 12AM and noon is at 12PM

### What does AM and PM even mean?

AM and PM are abbreviations, but what do they stand for.
One would naturally guess that AM stands for 'After Midday' (and PM for 'pefore midday' ?).
Sadly [AM stands for 'Ante Meridiem' and PM stands for 'Post Meridiem'](https://en.wikipedia.org/wiki/12-hour_clock).
This is the Latin for 'before midday' and 'after midday', but before is 'Ante' and after is 'Post' so we cannot even use the above guess to remember how the system works because it is the other way around.

Consider "9am", this means "nine hours before noon", but it is actually only three hours before noon.
Similarly, '12pm', which means "twelve hours after midday", is at noon and not twelve hours after noon.

### Twelve-hour time madness in a graph.

[12 PM is at noon. 12 AM is at midnight.](https://en.wikipedia.org/wiki/12-hour_clock#Confusion_at_noon_and_midnight)
Read that again.
The day starts with 12AM.
After 59 minutes, the time is 12:59AM.
That's 12 hours and 59 minutes, before noon.
The day is halfway at 12PM.
59 Minutes after that, the time is 12:59PM.
12 hours and 59 minutes, after noon.

What part of the day occurs before noon?
12:00AM - 12:59AM and 01:00AM - 11:59AM.
That's discontinuous!
In the following graph you can clearly see the madness.

![A graph of how 12h clocks work](/assets/the-annoyance-of-twelve-hour-clocks/12h_graph.png)

### The alternative

Twenty-four-hour time is an alternative to twelve-hour time and it is much more sensible.
In twenty-four-hour time, a day is divided up into 24 hours ranging from 0 to 23.
Midnight is at 00:00 and noon is at 12:00.
The day starts at 00:00 and ends at 23:59.
There is no AM/PM nonsense. 
Anything before 11:59 is before noon.
Anything after 12:00 is after noon.
Simple.

![A graph of how 24h clocks work](/assets/the-annoyance-of-twelve-hour-clocks/24h_graph.png)
