---
title: The quitting list
tags: quitting
---


2018 was as interesting year.
[I focused on my physical and mental health](/posts/2018-01-28-2017-year-in-review).
This year I will start sharing what I learnt by sharing what I call my 'quitting list'.

<div></div><!--more-->

This story starts in 2017.
I had no idea what being healthy meant but I finally had a bit more time to try and figure it out.
It turns out that being healthy is complicated, and I still do not know much about it, but I like to think that I have started moving in the right direction.

There are many things that one could do to start living a more healthy life.
I chose to focus on quitting unhealthy habits first.

I noticed that I would eat chocolate in very large amounts.
I could never eat just a little bit of chocolate.
It was either none at all, or a lot, and I could not trust myself with sweets in the house either.
I knew that I did not **want to want to** eat so much chocolate, but I did **want to**.
That is where I started.
Not knowing what healthy means, I started by quitting the things that I did not **want to want**.

Over time I figured out that there were many such things.
I started writing down the day after the last time I had any chocolate.
The first day I was clean, if you will.
I did this for everything I was quitting, and I would refresh the date if I relapsed.
This list grew slowly but surely.
As the date on the list would seem longer ago, I would be less likely to relapse.
Having been chocolate-free for longer only ever made me want to relapse even less.

After chocolate, I added many more bad habits to the list, starting with alcohol.
I call this list my quitting list.
I also added some items that I would never want to want to use, just to be sure that I would never start either.

In 2019, I plan to write about my quitting list and the specific items on it.
As a short overview, here is the current (summarised) list:

* Tabacco
* Weed
* Alcohol
* Sweets, Chocolate
* Facebook, Instagram, twitter, social media in general
* Coffee
* Meat
* Porn
* Sugar in processed foods, including soda.
* TV shows
* Animal products
* Gaming alone, including mobile gaming
* Movies alone
* Youtube alone
* All processed foods, including fruit juices
* Masturbation

I will probably not write about things I never quit because I never started, like tabacco and Weed, because all I can really say about them is "They seem like a bad idea".

The next post in this series will be about moderation, and why it does not work for me.
