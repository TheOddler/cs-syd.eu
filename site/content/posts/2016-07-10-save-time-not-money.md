---
layout: post
title: Save time, not money
tags: time,productivity
---

It may come natural to try and save money wherever we can, but I argue this may be a bad idea.

<div></div><!--more-->

Your time is finite and you cannot make more of it, so it makes sense to put a steep price on it.
This also means that, as long as you have the money, it is a good idea to spend money instead of time whenever the time you save is worth more than the money you spend to save it.

Spending money to save time (and effort) is not a new idea.
You are doing it already.

You could make your own clothes, but is easier and much less time consuming to buy the clothes instead.
You could build your house yourself, but it is easier and cheaper (in time) to buy/rent a house than to build it yourself.

In which situations you spend money instead of time is probably largely determined by how you brought up.
Chances are that you've never reconsidered in which situations it makes sense to spend time and save money or vice versa.

This reasoning is different for different people, so I will not use concrete numbers, but here are some obvious areas in which you should (re)consider your strategy.
Obviously you need to have the money before you can consider spending it instead of time, but that is also something you should decide for yourself.
These are roughly sorted by decreasing importance and increasing price.

- Food:

Growing food and cooking for yourself can save you a lot of money, but it one of the most time consuming activities on this list.
You have to eat very frequently and cooking takes a relatively large amount of time.
How much does it really cost to have money taken care of?


- Transport:

Walking everywhere is really cheap, but takes a long time.
Commuting by bicycle is marginally more expensive, depending on the bicycle, and takes at least three times less time.
Public transport is slightly more expensive and goes even faster, etc.
Figure out which trade-off works best for you.


- Cleaning:

Cleaning your accommodation is another time consuming activity.
Chances are you have never even considered using a cleaning service.
Have a look at cleaning services near you and decide whether it is worth it to clean your home yourself.


- Laundry:

If you've been brought up in a household that does its own laundry, you have probably never even considered using a laundry service.
Doing laundry can take up a lot of time, especially if you do not have your own laundry machines.
Have a look at laundry services near you before you decide whether using it is valuable to you.


- Travel arrangements:

If you travel a lot, chances are that you spend a lot of time arranging your travels. Consider whether it is worth it to have someone else to figure out the cheapest way to travel and to arrange those travels instead of doing that yourself.


- Personal management:

Figuring out who to meet and when is a time consuming practice in and of itself.
Maybe it is worth getting a secretary?


The list of areas where you can save time by spending money can go on much longer.
If you are really rich, it may be worth getting a chauffeur, a butler, a private helicopter with pilot?
This may sound ridiculous, but make sure you know where you draw the line and why, because you only have a finite (and small!) amount of time.

