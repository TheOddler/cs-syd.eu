---
layout: post
title: The switch to dvorak and an ergonomic keyboard
tags: dvorak, keyboard, ergonomy, TEK
---

I recently bought a ["Truly-Ergonomic Keyboard"](http://www.trulyergonomic.com).
It's an ergonomic mechanical keyboard designed to be comfortable and efficient.
At the same time I started to switch to a [Dvorak keyboard layout](http://en.wikipedia.org/wiki/Dvorak_Simplified_Keyboard).
Notice how I wrote "started to". I have already spent 10 hours trying to relearn how to type and I foresee many more of those hours.

<div></div><!--more-->

![The "truly ergonomic" keyboard](/assets/the-switch-to-dvorak-and-an-ergonomic-keyboard/keyboard.jpg)

The qwerty keyboard layout was designed to make sure that the typewriters of the time jammed as little as possible.
This meant that the most commonly used keys were placed as far apart as possible.
Obviously, this is exactly the opposite of what you would want in order to type quickly.
The Dvorak layout, on the other hand, is specifically designed to make it easy to type quickly and comfortably. 
Together with an ergonomic keyboard, this should form a perfect keyboard setup.

I used [an online tutorial](http://programmer-dvorak.appspot.com/index.html#lessons) to learn to type on a dvorak keyboard layout.
This tutorial does a good job teaching you letters two by two, instead of a whole row at a time, as most other practice tools do.
When I can type all letters adequately, I will switch to [GNU's gtypist](http://www.gnu.org/software/gtypist) as that tool focuses best on _accuracy_ in typing, before focusing on speed. 
I will use [an online test](http://10fastfingers.com/typing-test/english) to test my typing speed as I progress.

The first few hours I spent teaching myself a completely new layout were spent on learning the home row keys.
All vowels (except the 'y') are on the home row of the left hand (A-O-E-U-I).
The most commonly used consonants are on the home row of the right hand (D-H-T-N-S).
This way, as opposed to on the qwerty layout, you can already type a lot of words with only the letters on the home row.
I personally found the 'I' and 'D' keys the hardest to learn because they are on almost the opposite position from on the qwerty layout.
It helps to remember that all vowels are under the left hand.

After ten hours of [deliberate practice](http://www.farnamstreetblog.com/2012/07/what-is-deliberate-practice), I can now type with all letters, at _20_ words per minute. 
Keep in mind that, on a regular keyboard layout, my typing speed was around 70 words per minute.
I have set my wallpaper to a picture of my current layout for temporary reference, but I still switch back to a regular layout when I'm not practicing.

![My current keyboard layout](/assets/the-switch-to-dvorak-and-an-ergonomic-keyboard/layout.png)

I actually wrote this post on a dvorak layout.
As a result, it took me three times as long as any other post.

I will post an update as soon as I've reached my typing speed goal of 125 words per minute.
In the mean time, I urge everyone who types a lot to both buy an ergonomic keyboard an try the dvorak layout for a while.
The ergonomic keyboard reduces wrist and shoulder pains while typing on a dvorak layout is more comfortable in general.
