---
layout: post
title: "GTD with Taskwarrior, Part 1: Intro"
tags: Taskwarrior, productivity, work, GTD, tutorial, CLI, terminal, intro
---

This is the first post in a series where I'm going to show you how I use taskwarrior to implement GTD.
I would like to put up a guide that shows how to use taskwarrior to GTD in great detail, as well as make a record of my usage for personal recollection.

<div></div><!--more-->

This entire series assumes that you at least know what GTD is.
If you have no clue what GTD is, you can check out the blogpost [GTD in 15 minutes – A Pragmatic Guide to Getting Things Done](http://hamberg.no/gtd), or just read the book, of course.
The series focuses on *implementing* GTD, not explaining GTD, but you might be able to follow if you don't feel the need to know _why_ I do this.

In this post, I will show you what taskwarrior is, how to get it and the basics of using it.
Please note: If you want to replicate this system, you will have to be familiar with using a terminal.


### An intro to Taskwarrior

Taskwarrior is a *free and open-source*, *command-line*, *task management* program with options for *synchronisation*, *automation* and *optimisation*.
I think you can already see why I use this and why I would recommend using it.
There is a more extensive list [on the taskwarrior website](http://taskwarrior.org/docs/tutorials/start.html) of reasons to use taskwarrior, but I think I've made my point.

A Taskwarrior system can be viewed as a giant list of *all* your tasks.
Taskwarrior's strength comes in the abundancy of ways to view, create, update and check off these tasks.
In a way, it's just a very efficient list manager.


#### Getting Taskwarrior

Taskwarrior's `task` is a package in most Linux distributions.
I would recommend cloning the git repository and compiling the program from source, so as to get the latest version. (Taskwarrior is still very much under development)

More details on [how to get Taskwarrior](http://taskwarrior.org/download/) can be found on the website as well.
For me, installing `task` was as easy as running:

```
$ sudo pacman -S task
```

#### The basics

You can read the entire manual page for taskwarrior (`man task`), but it's probably easier to learn as you use the system.
The main concept to retain is that a `task` command is always of the same form:

```
$ task <filter> <command-name> <arguments>
```

The `<command-name>` part is obvious: You select which functionality you would use: `add` `done` `list`, etc...
A task command, by default, is run on _all your tasks_, but you use the filter part of the command to filter out the tasks that you want to run the command on.
The `<arguments>` part is different for every command, but you will get the hang of that soon.

The basic commands that you have to understand before you can start using your shiny new system are `next`, `add` and `done`.
Try running `task` right now.
You should see something like this:

```
$ task
[task next]
No matches.
```

Taskwarrior runs `task next` by default when you run `task`
`task next` shows you a list of your most urgent tasks.
A number of factors are used to determine urgency of a task.
For now, just assume that Taskwarrior knows what's best for you.
You will probably use this command most often.

Adding tasks is very easy:

```
$ task add Read chapter 1 of GTD
Created task 1.
```

Tadaah, you've created your first task.
Each task has an ID, this one has ID number 1.

Now, because you're using GTD, you will want to add the task to a project.
You do this by adding `project:read.gtd` to the command.
Because Taskwarrior is made for efficiency, you can shorten `project` to any unique prefix, so you can also use `pro:read.gtd`.
Taskwarrior also allows you to tag tasks.
You add a tag to a task with a `+<tag>` argument.

In full, you would use something like this:

```
$ task add Read chapter 1 of GTD pro:read.gtd +read +@home
```

Lastly, you need to be able to check off tasks.
Remember that each task has an ID?
You use that ID as a filter when you `done` a task.

```
$ task 1 done
```

This way, you can check off multiple tasks at once by enumerating them in a comma-separated list `1,3,8,71` or a contiguous set `1-5`, or both: `1,5-10,12`.

There, now you know the absolute basics.
If you want to read more on how to use taskwarrior, I suggest you read the [30-Second Tutorial](https://taskwarrior.org/docs/30second.html) on their website and the [Best Practices](http://taskwarrior.org/docs/best-practices.html).
