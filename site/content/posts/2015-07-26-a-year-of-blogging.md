---
layout: post
title: A year of blogging
tags: year, blogging, milestone, reflection
---

On 2014-07-29, my first ever blogpost went online.
That post is now a year old and I've written another post at least every week.
Let me reflect on a year of blogging.

<div></div><!--more-->

I started a blog to share interests, experiences and ideas, as well as to build a long term memory assistence tool.
The sharing did happen and I do now have a collection of articles that I wrote some time in the past but these, in hindsight, are not the interesting parts of blogging.

#### Time

I thought I would save time by blogging.
My reasoning was that I'd only have to write down my opinion on frequent subjects once and then I could give my opinion by referencing the post.
That did not work out as expected.
This solution looks like it scales very well (and it might!) but I don't have a big enough audience to make it worthwhile just yet.
As a temporary conclusion I'd say blogging does not save time.


#### Writing

There's one objective I had before I started that I did make very good progress on: I wanted to write better.
If I compare my first posts to my recent posts, I do seem to see a difference.
To get more significant results though, I would have to get my articles reviewed by good writers.


#### Unexpected advantages

There are also some advantages to blogging that I did not expect at all.
While writing, my own opinion often became clearer to myself than it was when I had it in my head.
Explaining an opinion in text rather than in speech forces you to form a better founded opinion because it's a lot harder to use ambiguity in your favor when writing than when talking.
This means that writing my opinion (and even more importantly: publishing my opinion) forced me to have really clear and valid arguments.

All in all, I'm very glad I started.
