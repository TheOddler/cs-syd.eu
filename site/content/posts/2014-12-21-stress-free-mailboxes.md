---
layout: post
title: Stress free mailboxes
tags: mail, stress, self-management
---

I've noticed people getting strangely annoyed when they receive an email.
They complain about mail being old-fashioned, unhandy, stressful, etc..
When I then look at their mailclient, I see something like this:

![An example of an overflowing mailbox](/assets/stress-free-mailboxes/exploding-inbox.png)

Honestly, if that was my inbox, I would be stressed out too.
I've since been trying to find a way to make an email-system stress-free, organised and efficient.

<div></div><!--more-->

### Getting it out of your inbox
The first step is to start seeing your inbox like you view your physical mailbox.
You never leave anything in there.
As soon as you get any physical mail, you take it out of your mailbox, and decide what to do with it.
That's exactly what you need to do with electronic mail too.

Even if it's just to empty your inbox, make another folder called "not-my-inbox" and drag your mail into it once you receive it.
At least then, you make a distinction between the mail that you have received and the mail that you've already seen, even if you don't have the time to read it right now.
This will make it easier to spot any new information without looking at the mails themselves.

### An archive
Next, you need to realise that there is no good reason to ever throw mail away (excluding spam, of course).
Storage is cheap, and you never know when you will need 'that mail you got after dinner at X in 2004'.

Most email providers also provide an 'archive' folder where all your received and sent mail is kept.
Gmail, for instance, has the 'All Mail' folder.
However, if you get a lot of mails, this archive might be too small for your purposes.

If you don't believe that you need an archive, delete all your mails right now and speak to me in a week.

### The four new folders
Here's what I suggest.
You create the following 4 email folders and you clear out your inbox into them whenever you receive mail.

- Read and Review: Any mail that you still have to read (again) before deciding what to do with it.
- Reply: Any mail that you still have to reply to.
- Waiting for: Any mail for which you're still waiting on some other person's action to continue.
- Reference: Any mail that contains information that you might still need in the future, but you don't have to do anything about.

The system then becomes something like this.

![A diagram of my proposed workflow](/assets/stress-free-mailboxes/workflow.png)

The above flowchart should be self-explanatory.

The entire point of these folders is to make sure there is no mail in your inbox, so that any mail in your inbox is a TODO-item.
The mails in your 'Read and Review' and 'Reply' folders are TODO-items too, but you have already decided that you won't be doing them right now and it's not new info.

### A final word
I realise that this system is not for everyone.
I'm just trying to show you an option.

You might not use email as a communicational tool.
You might not use email for work.
You might not even use email at all.
However, even if you use email on a regular basis and it's an essential tool for your work, you might not be stressed out by an overflowing mailbox.
Even in that case, I argue that you're better off finding some way to clean out your inbox.
Not only can you be more productive, your colleagues might be thankful that you handle email more professionally.


