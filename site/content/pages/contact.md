---
layout: page
title: Contact
header: Contact
description: Contact information and instructions
---

<script type="application/ld+json">
  {
    "@context": "http://schema.org/",
    "@type": "Person",
    "name": "Tom Sydney Kerckhove",
    "jobTitle": "Professional Weirdo",
    "url": "http://cs-syd.eu",
    "affiliation": "CS SYD",
    "birthDate": "1994-07-25",
    "alumniOf": "ETH Zuerich",
    "brand": "CS SYD",
    "gender": "male",
    "nationality": "Belgian"
  }
</script>



You can contact me via email at this address:

<div class="address">
  syd<span style="display:none">muhaha</span>@cs-syd.eu
</div>

<br>

I am not available via telephone.
