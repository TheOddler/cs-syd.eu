---
title: How you can make money off of hiring me
description: An overview of my consulting services
---

I do freelancing work as a technical leader.
I focus on improving the following aspects of software systems.

### Robustness

We expect software to work but software still misbehaves a lot.
I am passionate about systematic improvements to the robustness of systems.

### Ease of maintenance

Maintenance accounts for more than half of the cost of the software engineering process.
Optimising the ease of maintenance can have a significant impact on the cost of maintaining the system.

### Automation

Removing human involvement in a system is a great way to increase reliability and decrease costs.
This is why we build software in the first place.
When it comes to maintenance, however, automation is often overlooked.

<!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/cs-syd/introduction-call" style="min-width:320px;height:650px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->
