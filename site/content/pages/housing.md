---
title: Room for rent in a shared apartment.
description: An offer for a room for rent in a shared apartment
---

## Offer

* Free from: **2020-12-01**
* Price: **800 CHF** per month per room (all-in)
* Appartment Size: 100 m^2
* Rooms: 4.5
* Furniture: Common areas are furnished, furniture in the rooms is to be negotiated with the current tenants
* Period: Indefinite.
* Smoking: Not allowed
* Deposit: One month rent.
* Notice: 90 days every month


* Address: Altwiesenstrasse (For more details see below.)
* Public Transport Connections: Single tram to center (7) and Single tram to University (9).
* Time to ETH: 25 min
* Time to Center: 25 min
* Time to Hönggerberg: 30 min


* Dishwasher
* Gigabit internet connection
* Shared Washing room and drying room
* Bike Garage
* Storage room in the cellar and in the attic
* Bathrooms: 2 for 3 people
* Room size: 11 m^2

## Viewings

To organize a viewing, please send me an email.
[Click here to find my contact details.](/contact)
Please suggest a free time according to [my agenda](/agenda) and tell me a bit about yourself in this email as well.

For more information about me, look on this site.
 
You will get the full address when making an appointment.

## Pictures

## Living room & kitchen 

![Living room and kitchen](/assets/housing/2.jpg)

![Living room and kitchen](/assets/housing/3.jpg)

![Living room and kitchen](/assets/housing/4.jpg)

## Hallway

![Hallway](/assets/housing/5.jpg)

![Hallway](/assets/housing/6.jpg)

## Bathrooms

![Bathroom 1](/assets/housing/7.jpg)

![Bathroom 1](/assets/housing/8.jpg)

<!-- ## Room -->
<!--  -->
<!-- ![Room 1](/assets/housing/15.jpg) -->
<!-- ![Room 1](/assets/housing/16.jpg) -->
<!-- ![Room 1](/assets/housing/17.jpg) -->
<!-- ![Room 1](/assets/housing/18.jpg) -->
<!-- ![Room 1](/assets/housing/19.jpg) -->
<!-- ![Room 1](/assets/housing/20.jpg) -->
<!-- ![Room 1](/assets/housing/21.jpg) -->
<!-- ![Room 1](/assets/housing/22.jpg) -->
<!-- ![Room 1](/assets/housing/23.jpg) -->
<!-- ![Room 1](/assets/housing/24.jpg) -->
<!-- ![Room 1](/assets/housing/25.jpg) -->
<!-- ![Room 1](/assets/housing/26.jpg) -->
<!-- ![Room 1](/assets/housing/27.jpg) -->

## Room 

![Room 2](/assets/housing/room-2-6.jpg)
![Room 2](/assets/housing/room-2-7.jpg)
![Room 2](/assets/housing/room-2-8.jpg)
