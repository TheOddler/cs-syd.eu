---
title: Agenda
description: Instructions for making an appointment.
---

To make an appointment, please schedule a call via [calendly](https://calendly.com/cs-syd/) or send a calendar invite to `syd@cs-syd.eu`.

<!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/cs-syd/introduction-call" style="min-width:320px;height:650px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->


