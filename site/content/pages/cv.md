---
title: CV: Tom Sydney Kerckhove
description: An overview of my different CVs and highlights from them
---


![Headshot](/assets/speaking/headshot1.jpg)

Over time I have realised that having only one CV is quite limiting.
Different people have different expectations when it comes to a CV, so I made multiple CV documents:

- <a href="/cv/for-recruiters.pdf">A CV for recruiters</a>
- <a href="/cv/for-engineers.pdf">A CV for engineers</a>
- <a href="/cv/fail.pdf">A CV of Failures</a>
- <a href="/cv/cv.pdf">A regular CV</a>


### Highlights


- I have a masters degree from ETH with a specialisation in program analysis and synthesis
- I have a bachelors degree in informatics with a focus on abstract mathematics
- I have worked at huge companies: Google and Facebook, and very small companies
- [I have written over 100K LOC, most of which is public](https://github.com/NorfairKing/)
- I have got plenty of experience with writing:
  - [Around 200 blog posts](/archive)
  - Over 1000 pages of mathematics textbooks
  - [A paper (published on Arxiv)](https://arxiv.org/abs/1909.04160)
  - [A master thesis](https://cs-syd.eu/thesis)
- [I have plenty of experience with speaking](https://www.youtube.com/results?search_query=tom+sydney+kerckhove)
- I have taught programming to many students, both at university and in my spare time, as well as professionals
- I have done a lot of volunteering, for example as part of Google summer of code.

### Links

* [Github](https://github.com/NorfairKing)
* [Linkedin](https://www.linkedin.com/in/tomsydneykerckhove)
* [Email](/contact)


