---
title: Peer to Peer mentorship
description: My invitation for peer to peer mentorship
---

When it comes to learning, peer to peer mentorship can be very valuable.
Unfortunately, one-on-one time is in short supply.


These are the things that I am interested in mentoring in:

- Programming
- Maths
- Personal productivity
- Personal management
- Motivation
- Self-moderation

These are the things that I am interested in being mentored in:

- Health
- Exercise
- Ergonomics
- Nutrition
- Spoken Languages
- Advanced people skills

I am also always looking for more code reviewers.

Should you be interested in exchanging mentorship, please [contact me](/contact)
