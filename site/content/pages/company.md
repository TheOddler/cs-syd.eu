---
layout: page
title: Company
description: The CS SYD Company
---

<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Organization",
  "name": "CS SYD",
  "legalName": "CS Kerckhove",
  "address": {
    "addressCountry": "Switzerland",
    "addressLocality": "Zuerich",
    "addressRegion": "Zuerich",
    "postalCode": "8051",
    "streetAddress": "Altwiesenstrasse 93"
  },
  "founder": "Tom Sydney Kerckhove",
  "logo": "https://staging.cs-syd.eu/logo/res/positive/logo.png",
  "url": "https://cs-syd.eu"
}
</script>

<div class="columns is-centered">                
  <div class="column has-text-centered">
    <img src="/logo/res/positive/logo.png" alt="Logo" width="500px" max-width="100%"/>
  </div>
</div>

* Name: CS SYD
* Legal name: CS Kerckhove
* Address: Altwiesenstrasse 93, 8051 Zürich, Switzerland
