{ pkgs ? import ./nix/pkgs.nix
, sources ? import ./nix/sources.nix
, ...
}:
pkgs.haskell.lib.buildStackProject {
  name = "nixops-shell";
  buildInputs = with pkgs; [
    sass
    niv
    haskellPackages.linkcheck
    haskellPackages.seocheck
    zlib
  ];

  shellHook = ''
    export TERM=xterm
    export CV_DIR=${pkgs.cvRelease}

    function devel () {
      ./scripts/devel.sh
    }
  '';

}
