#!/usr/bin/env bash

set -e
set -x

cd site

if [ -z ${CV_DIR+x} ]
then
  export NO_CV=1
fi
if [ -z ${LOGO_DIR+x} ]
then
  export NO_LOGO=1
fi
if [ -z ${STYLE_FILE+x} ]
then
  export NO_STYLE=1
fi


stack install :site \
  --file-watch --watch-all \
  --exec="../scripts/redo-site.sh $@" \
  --ghc-options="-freverse-errors -DDEVELOPMENT -O0" \
  --fast \
  --pedantic \
  --no-nix-pure
