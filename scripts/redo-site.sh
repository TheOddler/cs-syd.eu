#!/usr/bin/env bash

set -x

PID="$(pgrep site)"
if [[ "$PID" != "" ]]
then
  kill $PID
  while [ -e /proc/$PID ]
  do
      echo "Process: $PID is still running"
      sleep .1
  done
fi

set -e

export DRAFTS=True

site serve $@ &
